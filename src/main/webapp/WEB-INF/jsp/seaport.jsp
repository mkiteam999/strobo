<%--
  Created by IntelliJ IDEA.
  User: MyKavalli
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>Seaport</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/seaport.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/owl.carousel.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body class="body-item seaport">
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
                <ul>
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#home' && menu.status == true}">
                            <li><a class="menu-item" data-menu="home" href="#home">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#about' && menu.status == true}">
                            <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#core' && menu.status == true}">
                            <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                    <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                        <ol class="submenu">
                            <c:forEach items="${menuDefault}" var="menu">
                                <c:if test="${menu.url == '#seaport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#manufacturing' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#warehouse' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#airport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                                </c:if>
                            </c:forEach>
                        </ol>
                    </li>

                    <c:forEach items="${menuNew}" var="menu">
                        <c:if test="${menu.status == true}">
                            <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                        </c:if>
                    </c:forEach>

                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#contact' && menu.status == true}">
                            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#news' && menu.status == true}">
                            <li><a class="menu-item" data-menu="news" href="${pageContext.request.contextPath}/news">News</a></li>
                        </c:if>
                    </c:forEach>
                    <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="body-content seaport">
        <div class="item-content active">
            <div class="all-item seaport active">
                <div class="seaport-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                <c:if test="${ not empty pageseaport.pageContent}">
                    <p>${pageseaport.pageContent}</p>
                </c:if>
                <c:if test="${ empty pageseaport.pageContent}">
                    <p>Advanced, integrated and automated horizontal container transportation solutions to meet the challenges of the seaport landscape and streamline the container handling process.</p>
                </c:if>
                    <a href="#" class="menu"></a>
                </div>
                <div class="seaport-content content">
                    <a href="${pageContext.request.contextPath}/services/airport" class="prev-detail-service"></a>
                    <c:if test="${ not empty pageseaport.pageName}">
                        <img class="bg" src="${pageContext.request.contextPath}/media/images/${pageseaport.pageName}.png">
                    </c:if>
                    <c:if test="${empty pageseaport.pageName}">
                        <img src="assets/all_seaport.png" alt="" class="bg">
                    </c:if>
                    <div class="seaport_1">
                        <div class="description">
                            <c:if test="${ not empty pageseaport1.content}">
                                <p class="right" type-for="seaport" data-img="seaport_1">${pageseaport1.content}</p>
                            </c:if>
                            <c:if test="${ empty pageseaport1.content}">
                                <p class="right">Electric Automated Guided Vehicle</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageseaport1.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageseaport1.pageName}.png" type-for="seaport" data-img="seaport_1">
                                </c:if>
                                <c:if test="${ empty pageseaport1.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/seaport_1.png" type-for="seaport" data-img="seaport_1">
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="seaport_2">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageseaport2.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageseaport2.pageName}.png" type-for="seaport" data-img="seaport_2">
                                </c:if>
                                <c:if test="${ empty pageseaport2.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/seaport_2.png" type-for="seaport" data-img="seaport_2">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${ not empty pageseaport2.content}">
                                <p type-for="seaport" data-img="seaport_2">${pageseaport2.content}</p>
                            </c:if>
                            <c:if test="${ empty pageseaport2.content}">
                                <p>Autonomous Prime Mover</p>
                            </c:if>
                        </div>
                    </div>
                    <div class="seaport_3">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageseaport3.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageseaport3.pageName}.png" type-for="seaport" data-img="seaport_3">
                                </c:if>
                                <c:if test="${ empty pageseaport3.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/seaport_3.png" type-for="seaport" data-img="seaport_3">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${ not empty pageseaport3.content}">
                                <p type-for="seaport" data-img="seaport_3">${pageseaport3.content}</p>
                            </c:if>
                            <c:if test="${ empty pageseaport3.content}">
                                <p>Electric Autonomous Prime Mover</p>
                            </c:if>
                        </div>
                    </div>
                    <a href="${pageContext.request.contextPath}/services/manufacturing" class="next-detail-service"></a>
                </div>
                <div class="all-footer footer">
                    <span class="first-detail" goto-for="seaport_1" ></span>
                    <h2>Seaport Solutions</h2>
                    <p class="go-rms">Robotics Management System</p>
                </div>
            </div>
        </div>
        <div class="item-details transition-down-hide">

            <!-- SEAPORT -->
            <div class="for-seaport for">
                <div da-slick="seaport_1" class="item seaport_1">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="seaport"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pageseaport1.listSlide) > 1}">
                                            <span class="prev-item" data-slide="seaport_1"></span>
                                        </c:if>
                                        <c:if test="${not empty pageseaport1.listSlide}">
                                            <c:set var="slide11" value="1"/>
                                            <c:forEach items="${pageseaport1.listSlide}" var="imgSlide11">
                                                <c:if test="${slide11 == 1}">
                                                    <img class="service-img main seaport_1" src="${pageContext.request.contextPath}/media/images/${imgSlide11.slideimg}">
                                                </c:if>
                                            <c:set var="slide11" value="${slide11 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pageseaport1.listSlide) > 1}">
                                            <span class="next-item" data-slide="seaport_1"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pageseaport1.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n11" value="1"/>
                                                <c:forEach items="${pageseaport1.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n11 == 1}">
                                                            <c:set var="addActive11" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n11}" type-click="seaport_1" class="img-thumbnail img-${n11} ${addActive11} seaport_1" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n11" value="${n11 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${ not empty pageseaport1.content}">
                                    <h2>${pageseaport1.content}</h2>
                                </c:if>
                                <c:if test="${ empty pageseaport1.content}">
                                    <h2>Electric Automated Guided Vehicle</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${ not empty pageseaport1.detailContent}">
                                    ${pageseaport1.detailContent}
                                </c:if>
                                <c:if test="${ empty pageseaport1.detailContent}">
                                    <ul>
                                        <li>Fully electric</li>
                                        <li>Integrated with natural navigation system</li>
                                        <li>Four wheel steer with a turning radius of 13m from wall to wall</li>
                                        <li>Versatile – able to carry 20ft/ 40/45 ISO containers</li>
                                        <li>Large Battery capacity of 180kwh</li>
                                        <li>Docking stop accuracy of ± 5 cm</li>
                                    </ul>
                                </c:if>
                                <c:if test="${not empty pageseaport1.brochure}">
                                    <div class="download">
                                        <a download href="/brochure/${pageseaport1.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                            <div class="watch-video"><a href="#" type-video="seaport_1">Watch video</a></div>
                            </div>
                        </div>
                        <div class="video" id="seaport_1">
                            <video id="player_seaport_1" class="video-seaport_1" controls width="100%">
                                <source src="${pageContext.request.contextPath}/media/video/seaport/seaport.mp4" type="video/mp4">
                            </video>
                            <div class="play-button"><span></span></div>
                            <p class="play-video" >Watch video</p>
                        </div>
                        <span class="next-detail" type-move="seaport"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="seaport_1" type-back="seaport"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="seaport_2" class="item seaport_2">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="seaport"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pageseaport2.listSlide) > 1}">
                                            <span class="prev-item" data-slide="seaport_2"></span>
                                        </c:if>
                                        <c:if test="${not empty pageseaport2.listSlide}">
                                            <c:set var="slide12" value="1"/>
                                            <c:forEach items="${pageseaport2.listSlide}" var="imgSlide12">
                                                <c:if test="${slide12 == 1}">
                                                    <img class="service-img main seaport_2" src="${pageContext.request.contextPath}/media/images/${imgSlide12.slideimg}">
                                                </c:if>
                                            <c:set var="slide12" value="${slide12 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pageseaport2.listSlide) > 1}">
                                            <span class="next-item" data-slide="seaport_2"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pageseaport2.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n12" value="1"/>
                                                <c:forEach items="${pageseaport2.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n12 == 1}">
                                                            <c:set var="addActive12" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n12}" type-click="seaport_2" class="img-thumbnail img-${n12} ${addActive12} seaport_2" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n12" value="${n12 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pageseaport2.content}">
                                    <h2>${pageseaport2.content}</h2>
                                </c:if>
                                <c:if test="${ empty pageseaport2.content}">
                                    <h2>Autonomous Prime Mover</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pageseaport2.detailContent}">
                                    ${pageseaport2.detailContent}
                                </c:if>
                                <c:if test="${ empty pageseaport2.detailContent}">
                                    <ul>
                                        <li>Diesel or fully electric</li>
                                        <li>Integrated with natural navigation system 170kw engine</li>
                                        <li>Versatile – able to carry 20ft/ 40/45 ISO containers</li>
                                        <li>Versatile – able to carry 20ft/ 40/45 ISO containers</li>
                                        <li>Large Battery capacity of 180kwh</li>
                                        <li>Docking stop accuracy of ± 5 cm</li>
                                    </ul>
                                </c:if>
                                <c:if test="${not empty pageseaport2.brochure}">
                                    <div class="download">
                                        <a download href="/brochure/${pageseaport2.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                            <div class="watch-video"><a href="#" type-video="seaport_2">Watch video</a></div>
                            </div>
                        </div>
                        <div class="video" id="seaport_2">
                            <video id="player_seaport_2" class="video-seaport_1" controls width="100%">
                                <source src="${pageContext.request.contextPath}/media/video/seaport/seaport.mp4" type="video/mp4">
                            </video>
                            <div class="play-button"><span></span></div>
                            <p class="play-video" >Watch video</p>
                        </div>
                        <span class="next-detail" type-move="seaport"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="seaport_2" type-back="seaport"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
                <div da-slick="seaport_3" class="item seaport_3">
                    <div class="detail-header header">
                        <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                        <a href="#" class="menu"></a>
                    </div>
                    <div class="detail-content">
                        <span class="pre-detail" type-move="seaport"></span>
                        <div class="view">
                            <div class="content-left">
                                <div>
                                    <div class="for-main">
                                        <c:if test="${fn:length(pageseaport3.listSlide) > 1}">
                                            <span class="prev-item" data-slide="seaport_3"></span>
                                        </c:if>
                                        <c:if test="${not empty pageseaport3.listSlide}">
                                            <c:set var="slide13" value="1"/>
                                            <c:forEach items="${pageseaport3.listSlide}" var="imgSlide13">
                                                <c:if test="${slide13 == 1}">
                                                    <img class="service-img main seaport_3" src="${pageContext.request.contextPath}/media/images/${imgSlide13.slideimg}">
                                                </c:if>
                                            <c:set var="slide13" value="${slide13 + 1}"/>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${fn:length(pageseaport3.listSlide) > 1}">
                                            <span class="next-item" data-slide="seaport_3"></span>
                                        </c:if>
                                    </div>
                                    <c:if test="${fn:length(pageseaport3.listSlide) > 1}">
                                        <div class="body-thumbnail">
                                            <div class="thumbnail owl-carousel">
                                                <c:set var="n13" value="1"/>
                                                <c:forEach items="${pageseaport3.listSlide}" var="imgSlide">
                                                    <c:if test="${not empty imgSlide}" >
                                                        <c:if test="${n13 == 1}">
                                                            <c:set var="addActive13" value="active"/>
                                                        </c:if>
                                                        <div class="around">
                                                            <img data-img="${n13}" type-click="seaport_3" class="img-thumbnail img-${n13} ${addActive13} seaport_3" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                        </div>
                                                        <c:set var="n13" value="${n13 + 1}"/>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                            <div class="content-right">
                                <c:if test="${not empty pageseaport3.content}">
                                    <h2>${pageseaport3.content}</h2>
                                </c:if>
                                <c:if test="${ empty pageseaport3.content}">
                                    <h2>Electric Autonomous Prime Mover</h2>
                                </c:if>
                                <p>KEY FEATURES</p>
                                <c:if test="${not empty pageseaport3.detailContent}">
                                    ${pageseaport3.detailContent}
                                </c:if>
                                <c:if test="${ empty pageseaport3.detailContent}">
                                    <ul>
                                        <li>Natural navigation system</li>
                                        <li>Full electric drive</li>
                                        <li>Versatile – able to carry 20ft/ 40/45 ISO containers</li>
                                        <li>Large Battery capacity of 180kwh</li>
                                        <li>Docking stop accuracy of ± 5 cm</li>
                                    </ul>
                                </c:if>
                                <c:if test="${not empty pageseaport3.brochure}">
                                    <div class="download">
                                        <a download href="/brochure/${pageseaport3.brochure}">Download brochure</a>
                                    </div>
                                </c:if>
                            <div class="watch-video"><a href="#" type-video="seaport_3">Watch video</a></div>
                            </div>
                        </div>
                        <div class="video" id="seaport_3">
                            <video id="player_seaport_3" class="video-seaport_1" controls width="100%">
                                <source src="${pageContext.request.contextPath}/media/video/seaport/seaport.mp4" type="video/mp4">
                            </video>
                            <div class="play-button"><span></span></div>
                            <p class="play-video" >Watch video</p>
                        </div>
                        <span class="next-detail" type-move="seaport"></span>
                    </div>
                    <div class="detail-footer footer">
                        <span class="back detail-back" num-video="seaport_3" type-back="seaport"></span>
                        <span class="scroll-down">scroll down</span>
                    </div>
                </div>
            </div>
            <!-- END SEAPORT -->

            <!-- RMS -->

            <div class="item rms">
                <div class="detail-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                    <a href="#" class="menu"></a>
                </div>
                <div class="detail-content">
                    <div class="view">
                        <div class="content-left">
                            <div>
                                <img class="service-img main" src="${pageContext.request.contextPath}/assets/detail_rms.png">
                            </div>
                        </div>
                        <div class="content-right">
                            <c:if test="${empty robotic.name}">
                                <h2>Robotics Management System</h2>
                            </c:if>
                            <c:if test="${not empty robotic.name}">
                                <h2>${robotic.name}</h2>
                            </c:if>
                            <p>DESCRIPTION</p>
                            <c:if test="${empty robotic.description}">
                                <h5>Harnessing the power of technology and smart engineering, every STROBO autonomous solution is integrated with ST Engineering’s proprietary Robotics Management System (RMS) which allows seamless operation and fleet management in existing infrastructure.</h5>
                            </c:if>
                            <c:if test="${not empty robotic.description}">
                                <h5>${robotic.description}</h5>
                            </c:if>
                            <p>KEY FEATURES</p>
                            <c:if test="${not empty robotic.keyfeatures}">
                                ${robotic.keyfeatures}
                            </c:if>
                            <c:if test="${ empty robotic.keyfeatures}">
                                <ul>
                                    <li>Real-time platform status</li>
                                    <li>Fail-safe disabling</li>
                                    <li>Interface for mobility mode control</li>
                                    <li>Interface for payload control</li>
                                    <li>Manned to unmanned switching</li>
                                    <li>Linked to existing infrastructure</li>
                                    <li>Collaborative networking</li>
                                    <li>Wireless transmission Cybersecure</li>
                                </ul>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="detail-footer footer">
                    <span class="back detail-back" num-video="rms" type-back="seaport"></span>
                    <span class="scroll-down">scroll down</span>
                </div>
            </div>
            <!-- END RMS -->

        </div>
        <div class="all-footers footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>