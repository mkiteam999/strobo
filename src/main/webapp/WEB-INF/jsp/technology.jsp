
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <%--<!-- <meta name="keywords" content="${homePage.keyword}"> -->--%>
    <%--<!-- <meta name="description" content="${homePage.meta}"> -->--%>
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <%--<!-- <c:if test="${empty homePage.title}" > -->--%>
        <title>Our Core Technology</title>
    <%--<!-- </c:if> -->--%>
    <%--<!-- <c:if test="${not empty homePage.title}" > -->--%>
        <%--<!-- <title>${homePage.title}</title> -->--%>
    <%--<!-- </c:if> -->--%>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/technology.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/mobile.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body>
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
                <ul>
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#home' && menu.status == true}">
                            <li><a class="menu-item" data-menu="home" href="#home">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#about' && menu.status == true}">
                            <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#core' && menu.status == true}">
                            <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                    <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                        <ol class="submenu">
                            <c:forEach items="${menuDefault}" var="menu">
                                <c:if test="${menu.url == '#seaport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#manufacturing' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#warehouse' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#airport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                                </c:if>
                            </c:forEach>
                        </ol>
                    </li>

                    <c:forEach items="${menuNew}" var="menu">
                        <c:if test="${menu.status == true}">
                            <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                        </c:if>
                    </c:forEach>

                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#contact' && menu.status == true}">
                            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#news' && menu.status == true}">
                            <li><a class="menu-item" data-menu="news" href="${pageContext.request.contextPath}/news">News</a></li>
                        </c:if>
                    </c:forEach>
                    <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-header">
        <a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/assets/logo.png" class="logo"></a>
        <a href="#" class="menu"></a>
    </div>
    <div class="core-tech">
        <div class="core-header header">
            <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img src="${pageContext.request.contextPath}/assets/logo.png" class="logo"></a>
            <img src="${pageContext.request.contextPath}/assets/menu_black.png" class="menu">
        </div>
        <div class="core-content">
            <p class="intro">our core technologies</p>
            <div class="details">
                
                <c:if test="${not empty technology.heading1}">
                    <h2>${technology.heading1}</h2>
                </c:if>
                <c:if test="${ empty technology.heading1}">
                    <h2>LIDAR SLAM Technology</h2>
                </c:if>
                
                <c:if test="${not empty technology.description1}">
                    <h5>${technology.description1}</h5>
                </c:if>
                <c:if test="${ empty technology.description1}">
                    <h5>Integral for dynamic path planning, natural navigation, obstacle/collision detection and avoidance. LIDAR SLAM technology enables STROBO’s suite of fully Autonomous Material Handling Equipment to handle routine, repetitive material handling tasks in a fast, safe and efficient manner.</h5>
                </c:if>
                <c:if test="${not empty technology.heading2}">
                    <h2>${technology.heading2}</h2>
                </c:if>
                <c:if test="${ empty technology.heading2}">
                   <h2>Robotics Management System</h2>
                </c:if>
                <c:if test="${not empty technology.description2}">
                    <h5>${technology.description2}</h5>
                </c:if>
                <c:if test="${ empty technology.description2}">
                  <h5>ST Engineering’s proprietary and customisable Robotics Management System (RMS) enables seamless integration and central control of multiple STROBO units within the customer’s environment (e.g. warehouse). This makes them capable of natural navigation without the use of any infrastructure by incorporating LIDAR SLAM technology for dynamic path planning, obstacle/collision detection and avoidance. STROBO units are also enabled to operate with cargo lifts and high speed doors to provide inter-building delivery services. With advanced pallet detection and recognition capability, customers can look forward to high throughput with great accuracy and easy integration with existing operations.</h5>
                </c:if>
            </div>
            <c:if test="${not empty lsFeatures}">
                <ul class="tech">
                    <c:forEach items="${lsFeatures}" var="feature">
                        <li>
                            <img src="media/images/${feature.featureimage}" alt="">
                            <p>${feature.featurename}</p>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>
        </div>
        <div class="core-footer footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>