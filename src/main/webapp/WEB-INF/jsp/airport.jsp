<%--
  Created by IntelliJ IDEA.
  User: MyKavalli
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>Airport</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/airport.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/owl.carousel.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
</head>
<body class="body-item airport">
    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
                <ul>
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#home' && menu.status == true}">
                            <li><a class="menu-item" data-menu="home" href="#home">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#about' && menu.status == true}">
                            <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#core' && menu.status == true}">
                            <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                    <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                        <ol class="submenu">
                            <c:forEach items="${menuDefault}" var="menu">
                                <c:if test="${menu.url == '#seaport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#manufacturing' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#warehouse' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#airport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                                </c:if>
                            </c:forEach>
                        </ol>
                    </li>

                    <c:forEach items="${menuNew}" var="menu">
                        <c:if test="${menu.status == true}">
                            <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                        </c:if>
                    </c:forEach>

                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#contact' && menu.status == true}">
                            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#news' && menu.status == true}">
                            <li><a class="menu-item" data-menu="news" href="${pageContext.request.contextPath}/news">News</a></li>
                        </c:if>
                    </c:forEach>
                    <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="body-content airport">
        <div class="item-content active">
            <div class="all-item airport active">
                <div class="airport-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                    <c:if test="${ not empty pageairport.pageContent}" >
                        <p>${pageairport.pageContent}</p>
                    </c:if>
                    <c:if test="${ empty pageairport.pageContent}" >
                        <p>Integrated, innovative automated solutions for both terminal and airside to realise the vision of the world’s future airports, powered by enhanced safety, productivity and efficiency.</p>
                    </c:if>
                    <a href="#" class="menu"></a>
                </div>
                <div class="airport-content content">
                    <a href="${pageContext.request.contextPath}/services/warehouse" class="prev-detail-service"></a>
                    <c:if test="${ not empty pageairport.pageName}">
                        <img class="bg" src="${pageContext.request.contextPath}/media/images/${pageairport.pageName}.png">
                    </c:if>
                    <c:if test="${ empty pageairport.pageName}">
                        <img src="assets/all_airport.png" alt="" class="bg">
                    </c:if>
                    <div class="airport_1">
                        <div class="description">
                            <c:if test="${not empty pageairport1.content}" >
                                <p type-for="airport" data-img="airport_1" class="right">${pageairport1.content}</p>
                            </c:if>
                            <c:if test="${ empty pageairport1.content}">
                                <p class="right">Autonomous Mini Bus</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageairport1.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageairport1.pageName}.png" type-for="airport" data-img="airport_1">
                                </c:if>
                                <c:if test="${ empty pageairport1.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/airport_1.png" data-img="airport_1">
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="airport_2">
                        <div class="description">
                            <c:if test="${not empty pageairport2.content}" >
                                <p type-for="airport" data-img="airport_2" class="right">${pageairport2.content}</p>
                            </c:if>
                            <c:if test="${empty pageairport2.content}" >
                                <p class="right">Autonomous Baggae Tractor</p>
                            </c:if>
                        </div>
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageairport2.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageairport2.pageName}.png" type-for="airport" data-img="airport_2">
                                </c:if>
                                <c:if test="${ empty pageairport2.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/airport_2.png" data-img="airport_2">
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="airport_3">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageairport3.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageairport3.pageName}.png" type-for="airport" data-img="airport_3">
                                </c:if>
                                <c:if test="${ empty pageairport3.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/airport_3.png" data-img="airport_3">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${not empty pageairport3.content}" >
                                <p type-for="airport" data-img="airport_3">${pageairport3.content}</p>
                            </c:if>
                            <c:if test="${ empty pageairport3.content}" >
                                <p>Folo Wheelchair</p>
                            </c:if>
                        </div>
                    </div>
                    <div class="airport_4">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageairport4.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageairport4.pageName}.png" type-for="airport" data-img="airport_4">
                                </c:if>
                                <c:if test="${ empty pageairport4.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/airport_4.png" data-img="airport_4">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${not empty pageairport4.content}">
                                <p type-for="airport" data-img="airport_4">${pageairport4.content}</p>
                            </c:if>
                            <c:if test="${empty pageairport4.content}">
                                <p>Autonomous Security Robot</p>
                            </c:if>
                        </div>
                    </div>
                    <div class="airport_5">
                        <div class="image">
                            <div class="table-cell">
                                <c:if test="${ not empty pageairport5.pageName}">
                                    <img src="${pageContext.request.contextPath}/media/images/${pageairport5.pageName}.png" type-for="airport" data-img="airport_5">
                                </c:if>
                                <c:if test="${ empty pageairport5.pageName}">
                                    <img src="${pageContext.request.contextPath}/assets/airport_5.png" data-img="airport_5">
                                </c:if>
                            </div>
                        </div>
                        <div class="description">
                            <c:if test="${not empty pageairport5.content}">
                                <p type-for="airport" data-img="airport_5">${pageairport5.content}</p>
                            </c:if>
                            <c:if test="${empty pageairport5.content}">
                                <p>Autonomous Passenger Loading Bridge</p>
                            </c:if>
                        </div>
                    </div>
                    <a href="${pageContext.request.contextPath}/services/seaport" class="next-detail-service"></a>
                </div>
                <div class="all-footer footer">
                    <span class="first-detail" goto-for="airport_1" ></span>
                    <h2>Airport Solutions</h2>
                    <p class="go-rms">Robotics Management System</p>
                </div>
            </div>
        </div>
        <div class="item-details transition-down-hide">

                <!-- AIRPORT -->
                <div class="for-airport for">
                    <div da-slick="airport_1" class="item airport_1">
                        <div class="view">
                            <div class="detail-header header">
                                <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                                <a href="#" class="menu"></a>
                            </div>
                            <div class="detail-content">
                                <span class="pre-detail" type-move="airport"></span>
                                <div class="view">
                                    <div class="content-left">
                                        <div>
                                            <div class="for-main">
                                                <c:if test="${fn:length(pageairport1.listSlide) > 1}">
                                                    <span class="prev-item" data-slide="airport_1"></span>
                                                </c:if>
                                                <c:if test="${not empty pageairport1.listSlide}">
                                                    <c:set var="slide41" value="1"/>
                                                    <c:forEach items="${pageairport1.listSlide}" var="imgSlide41">
                                                        <c:if test="${slide41 == 1}">
                                                            <img class="service-img main airport_1" src="${pageContext.request.contextPath}/media/images/${imgSlide41.slideimg}">
                                                        </c:if>
                                                    <c:set var="slide41" value="${slide41 + 1}"/>
                                                    </c:forEach>
                                                </c:if>
                                                <c:if test="${fn:length(pageairport1.listSlide) > 1}">
                                                    <span class="next-item" data-slide="airport_1"></span>
                                                </c:if>
                                            </div>
                                            <c:if test="${fn:length(pageairport1.listSlide) > 1}">
                                                <div class="body-thumbnail">
                                                    <div class="thumbnail owl-carousel">
                                                        <c:set var="n41" value="1"/>
                                                        <c:forEach items="${pageairport1.listSlide}" var="imgSlide">
                                                            <c:if test="${not empty imgSlide}" >
                                                                <c:if test="${n41 == 1}">
                                                                    <c:set var="addActive41" value="active"/>
                                                                </c:if>
                                                                <div class="around">
                                                                    <img data-img="${n41}" type-click="airport_1" class="img-thumbnail img-${n41} ${addActive41} airport_1" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                                </div>
                                                                <c:set var="n41" value="${n41 + 1}"/>
                                                            </c:if>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="content-right">
                                        <c:if test="${not empty pageairport1.content}">
                                            <h2>${pageairport1.content}</h2>
                                        </c:if>
                                        <c:if test="${ empty pageairport1.content}">
                                            <h2>Autonomous Mini Bus</h2>
                                        </c:if>
                                        
                                        <p>KEY FEATURES</p>
                                        <c:if test="${not empty pageairport1.detailContent}">
                                            ${pageairport1.detailContent}
                                        </c:if>
                                        <c:if test="${ empty pageairport1.detailContent}">
                                            <ul>
                                                <li>Multi-layer safety management</li>
                                                <li>Vehicle platform agnostic</li>
                                                <li>Inclement weather operations</li>
                                                <li>Onboard diagnostic software</li>
                                                <li>Embedded cybersecurity</li>
                                                <li>Over-the-air updates and remote control</li>
                                                <li>SAE Level 4 autonomy</li>
                                            </ul>
                                        </c:if>
                                        <c:if test="${empty pageairport1.brochure}">
                                            <div class="download">
                                                <a href="#"></a>
                                            </div>
                                        </c:if>
                                     
                                        <c:if test="${not empty pageairport1.brochure}">
                                            <div class="download">
                                                <a download href="/brochure/${pageairport1.brochure}"></a>
                                            </div>
                                        </c:if>
                                        <div class="watch-video"><a href="#" type-video="airport_1">Watch video</a></div>
                                    </div>
                                </div>
                                <div class="video">
                                    <video id="player_airport_1" class="video-airport_1" controls width="100%" title="">
                                        <source src="${pageContext.request.contextPath}/media/video/airport/airport.mp4" type="video/mp4">
                                    </video>
                                    <div class="play-button"><span></span></div>
                                    <p class="play-video" >Watch video</p>
                                </div>
                                <span class="next-detail" type-move="airport"></span>
                            </div>
                        </div>
                        <div class="detail-footer footer">
                            <span class="back detail-back" num-video="airport_1" type-back="airport"></span>
                            <span class="scroll-down">scroll down</span>
                        </div>
                    </div>
                    <div da-slick="airport_2" class="item airport_2">
                        <div class="detail-header header">
                            <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                            <a href="#" class="menu"></a>
                        </div>
                        <div class="detail-content">
                            <span class="pre-detail" type-move="airport"></span>
                            <div class="view">
                                <div class="content-left">
                                    <div>
                                        <div class="for-main">
                                            <c:if test="${fn:length(pageairport2.listSlide) > 1}">
                                                <span class="prev-item" data-slide="airport_2"></span>
                                            </c:if>
                                            <c:if test="${not empty pageairport2.listSlide}">
                                                <c:set var="slide42" value="1"/>
                                                <c:forEach items="${pageairport2.listSlide}" var="imgSlide42">
                                                    <c:if test="${slide42 == 1}">
                                                        <img class="service-img main airport_2" src="${pageContext.request.contextPath}/media/images/${imgSlide42.slideimg}">
                                                    </c:if>
                                                <c:set var="slide42" value="${slide42 + 1}"/>
                                                </c:forEach>
                                            </c:if>
                                            <c:if test="${fn:length(pageairport2.listSlide) > 1}">
                                                <span class="next-item" data-slide="airport_2"></span>
                                            </c:if>
                                        </div>
                                        <c:if test="${fn:length(pageairport2.listSlide) > 1}">
                                            <div class="body-thumbnail">
                                                <div class="thumbnail owl-carousel">
                                                    <c:set var="n42" value="1"/>
                                                    <c:forEach items="${pageairport2.listSlide}" var="imgSlide">
                                                        <c:if test="${not empty imgSlide}" >
                                                            <c:if test="${n42 == 1}">
                                                                <c:set var="addActive42" value="active"/>
                                                            </c:if>
                                                            <div class="around">
                                                                <img data-img="${n42}" type-click="airport_2" class="img-thumbnail img-${n42} ${addActive42} airport_2" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                            </div>
                                                            <c:set var="n42" value="${n42 + 1}"/>
                                                        </c:if>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="content-right">
                                    <c:if test="${not empty pageairport2.content}">
                                        <h2>${pageairport2.content}</h2>
                                    </c:if>
                                    <c:if test="${ empty pageairport2.content}">
                                        <h2>Autonomous Baggage Tractor</h2>
                                    </c:if>
                                    <p>KEY FEATURES</p>
                                    <c:if test="${not empty pageairport2.detailContent}">
                                        ${pageairport2.detailContent}
                                    </c:if>
                                    <c:if test="${ empty pageairport2.detailContent}">
                                        <ul>
                                            <li>Able to support a variety of payload including baggage trolly and ULD Dolly</li>
                                            <li>Up to 15 ton towing capacity</li>
                                            <li>Speeds up to 20 km/h</li>
                                            <li>30 km range</li>
                                            <li>Advance laser-based SLAM navigation technology</li>
                                            <li>Infrastructure-less – Does not require reflectors or magnetic strips for guidance</li>
                                            <li>Quickly redeployable to different locations Suitable for both indoor and outdoor operations</li>
                                            <li>Detects and avoids obstacles in real-time</li>
                                            <li>Safe and repeatable</li>
                                            <li>Autonomous charging capability</li>
                                            <li>Centrally managed through Robotics Management</li>
                                            <li>System and interfaces with terminal control system</li>
                                        </ul>
                                    </c:if>
                                        <c:if test="${not empty pageairport2.brochure}">
                                            <div class="download">
                                                <a download href="/brochure/${pageairport2.brochure}">Download brochure</a>
                                            </div>
                                        </c:if>
                                    <div class="watch-video"><a href="#" type-video="airport_2">Watch video</a></div>
                                </div>
                            </div>
                            <div class="video" id="airport_2">
                                <video id="player_airport_2" class="video-airport_1" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/airport/airport.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                            <span class="next-detail" type-move="airport"></span>
                        </div>
                        <div class="detail-footer footer">
                            <span class="back detail-back" num-video="airport_2" type-back="airport"></span>
                            <span class="scroll-down">scroll down</span>
                        </div>
                    </div>
                    <div da-slick="airport_3" class="item airport_3">
                        <div class="detail-header header">
                            <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png" /></a>
                            <a href="#" class="menu"></a>
                        </div>
                        <div class="detail-content">
                            <span class="pre-detail" type-move="airport"></span>
                            <div class="view">
                                <div class="content-left">
                                    <div>
                                        <div class="for-main">
                                            <c:if test="${fn:length(pageairport3.listSlide) > 1}">
                                                <span class="prev-item" data-slide="airport_3"></span>
                                            </c:if>
                                            <c:if test="${not empty pageairport3.listSlide}">
                                                <c:set var="slide43" value="1"/>
                                                <c:forEach items="${pageairport3.listSlide}" var="imgSlide43">
                                                    <c:if test="${slide43 == 1}">
                                                        <img class="service-img main airport_3" src="${pageContext.request.contextPath}/media/images/${imgSlide43.slideimg}">
                                                    </c:if>
                                                <c:set var="slide43" value="${slide43 + 1}"/>
                                                </c:forEach>
                                            </c:if>
                                            <c:if test="${fn:length(pageairport3.listSlide) > 1}">
                                                <span class="next-item" data-slide="airport_3"></span>
                                            </c:if>
                                        </div>
                                        <c:if test="${fn:length(pageairport3.listSlide) > 1}">
                                            <div class="body-thumbnail">
                                                <div class="thumbnail owl-carousel">
                                                    <c:set var="n43" value="1"/>
                                                    <c:forEach items="${pageairport3.listSlide}" var="imgSlide">
                                                        <c:if test="${not empty imgSlide}" >
                                                            <c:if test="${n43 == 1}">
                                                                <c:set var="addActive43" value="active"/>
                                                            </c:if>
                                                            <div class="around">
                                                                <img data-img="${n43}" type-click="airport_3" class="img-thumbnail img-${n43} ${addActive43} airport_3" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                            </div>
                                                            <c:set var="n43" value="${n43 + 1}"/>
                                                        </c:if>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="content-right">
                                    <c:if test="${not empty pageairport3.content}">
                                        <h2>${pageairport3.content}</h2>
                                    </c:if>
                                    <c:if test="${ empty pageairport3.content}">
                                        <h2>Folo Wheelchair</h2>
                                    </c:if>
                                    
                                    <p>KEY FEATURES</p>
                                    <c:if test="${not empty pageairport3.detailContent}">
                                        ${pageairport3.detailContent}
                                    </c:if>
                                    <c:if test="${ empty pageairport3.detailContent}">
                                        <ul>
                                            <li>Semi-Autonomous Follow-Me technology which tracks and follow an individual staff member</li>
                                            <li>Can be daisy chained to transport several users at once</li>
                                            <li>Quickly redeployable to different locations</li>
                                            <li>Suitable for both indoor and outdoor operations</li>
                                            <li>Detects and avoids obstacles in real-time</li>
                                            <li>Safe and reliable Wireless charging capability</li>
                                            <li>Can be monitored through Robotics Management</li>
                                            <li>System and interfaces with terminal control system</li>
                                        </ul>
                                    </c:if>

                                        <c:if test="${not empty pageairport3.brochure}">
                                            <div class="download">
                                                <a download href="/brochure/${pageairport3.brochure}">Download brochure</a>
                                            </div>
                                        </c:if>
                                        <div class="watch-video"><a href="#" type-video="airport_3">Watch video</a></div>
                                </div>
                            </div>
                            <div class="video" id="airport_3">
                                <video id="player_airport_3" class="video-airport_1" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/airport/airport.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                            <span class="next-detail" type-move="airport"></span>
                        </div>
                        <div class="detail-footer footer">
                            <span class="back detail-back" num-video="airport_3" type-back="airport"></span>
                            <span class="scroll-down">scroll down</span>
                        </div>
                    </div>
                    <div da-slick="airport_4" class="item airport_4">
                        <div class="detail-header header">
                            <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                            <a href="#" class="menu"></a>
                        </div>
                        <div class="detail-content">
                            <span class="pre-detail" type-move="airport"></span>
                            <div class="view">
                                <div class="content-left">
                                    <div>
                                        <div class="for-main">
                                            <c:if test="${fn:length(pageairport4.listSlide) > 1}">
                                                <span class="prev-item" data-slide="airport_4"></span>
                                            </c:if>
                                            <c:if test="${not empty pageairport4.listSlide}">
                                                <c:set var="slide44" value="1"/>
                                                <c:forEach items="${pageairport4.listSlide}" var="imgSlide44">
                                                    <c:if test="${slide44 == 1}">
                                                        <img class="service-img main airport_4" src="${pageContext.request.contextPath}/media/images/${imgSlide44.slideimg}">
                                                    </c:if>
                                                <c:set var="slide44" value="${slide44 + 1}"/>
                                                </c:forEach>
                                            </c:if>
                                            <c:if test="${fn:length(pageairport4.listSlide) > 1}">
                                                <span class="next-item" data-slide="airport_4"></span>
                                            </c:if>
                                        </div>
                                        <c:if test="${fn:length(pageairport4.listSlide) > 1}">
                                            <div class="body-thumbnail">
                                                <div class="thumbnail owl-carousel">
                                                    <c:set var="n44" value="1"/>
                                                    <c:forEach items="${pageairport4.listSlide}" var="imgSlide">
                                                        <c:if test="${not empty imgSlide}" >
                                                            <c:if test="${n44 == 1}">
                                                                <c:set var="addActive44" value="active"/>
                                                            </c:if>
                                                            <div class="around">
                                                                <img data-img="${n44}" type-click="airport_4" class="img-thumbnail img-${n44} ${addActive44} airport_4" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                            </div>
                                                            <c:set var="n44" value="${n44 + 1}"/>
                                                        </c:if>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="content-right">
                                    <c:if test="${not empty pageairport4.content}">
                                        <h2>${pageairport4.content}</h2>
                                    </c:if>
                                    <c:if test="${ empty pageairport4.content}">
                                        <h2>Autonomous Security Robot</h2>
                                    </c:if>
                                    <p>KEY FEATURES</p>
                                    <c:if test="${not empty pageairport4.content}">
                                        ${pageairport4.detailContent}
                                    </c:if>
                                    <c:if test="${ empty pageairport4.content}">
                                        <ul>
                                            <li>Autonomous navigation and obstacle avoidance</li>
                                            <li>Remote tele-operation</li>
                                            <li>Real-time all-round video surveillance</li>
                                            <li>Facial suspect list recognition</li>
                                            <li>Secured 4G/LTE network compatible</li>
                                        </ul>
                                    </c:if>
                                        <c:if test="${not empty pageairport4.brochure}">
                                            <div class="download">
                                                <a download href="/brochure/${pageairport4.brochure}">Download brochure</a>
                                            </div>
                                        </c:if>
                                        <div class="watch-video"><a href="#" type-video="airport_4">Watch video</a></div>
                                </div>
                            </div>
                            <div class="video" id="airport_4">
                                <video id="player_airport_4" class="video-airport_1" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/airport/airport.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                            <span class="next-detail" type-move="airport"></span>
                        </div>
                        <div class="detail-footer footer">
                            <span class="back detail-back" num-video="airport_4" type-back="airport"></span>
                            <span class="scroll-down">scroll down</span>
                        </div>
                    </div>
                    <div da-slick="airport_5" class="item airport_5">
                        <div class="detail-header header">
                            <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                            <a href="#" class="menu"></a>
                        </div>
                        <div class="detail-content">
                            <span class="pre-detail" type-move="airport"></span>
                            <div class="view">
                                <div class="content-left">
                                    <div>
                                        <div class="for-main">
                                            <c:if test="${fn:length(pageairport5.listSlide) > 1}">
                                                <span class="prev-item" data-slide="airport_5"></span>
                                            </c:if>
                                            <c:if test="${not empty pageairport5.listSlide}">
                                                <c:set var="slide45" value="1"/>
                                                <c:forEach items="${pageairport5.listSlide}" var="imgSlide45">
                                                    <c:if test="${slide45 == 1}">
                                                        <img class="service-img main airport_5" src="${pageContext.request.contextPath}/media/images/${imgSlide45.slideimg}">
                                                    </c:if>
                                                <c:set var="slide45" value="${slide45 + 1}"/>
                                                </c:forEach>
                                            </c:if>
                                            <c:if test="${fn:length(pageairport5.listSlide) > 1}">
                                                <span class="next-item" data-slide="airport_5"></span>
                                            </c:if>
                                        </div>
                                        <c:if test="${fn:length(pageairport5.listSlide) > 1}">
                                            <div class="body-thumbnail">
                                                <div class="thumbnail owl-carousel">
                                                    <c:set var="n45" value="1"/>
                                                    <c:forEach items="${pageairport5.listSlide}" var="imgSlide">
                                                        <c:if test="${not empty imgSlide}" >
                                                            <c:if test="${n45 == 1}">
                                                                <c:set var="addActive45" value="active"/>
                                                            </c:if>
                                                            <div class="around">
                                                                <img data-img="${n45}" type-click="airport_5" class="img-thumbnail img-${n45} ${addActive45} airport_5" src="${pageContext.request.contextPath}/media/images/${imgSlide.slideimg}">
                                                            </div>
                                                            <c:set var="n45" value="${n45 + 1}"/>
                                                        </c:if>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="content-right">
                                    <c:if test="${not empty pageairport5.content}">
                                        <h2>${pageairport5.content}</h2>
                                    </c:if>
                                    <c:if test="${ empty pageairport5.content}">
                                        <h2>Autonomous Passenger Loading Bridge</h2>
                                    </c:if>
                                    <p>KEY FEATURES</p>
                                    <c:if test="${not empty pageairport5.content}">
                                        ${pageairport5.detailContent}
                                    </c:if>
                                    <c:if test="${ empty pageairport5.content}">
                                        <ul>
                                            <li>Fully Automated Aerobridge Docking System</li>
                                            <li>Autonomous docking capabilities</li>
                                            <li>One-touch docking using the Remote Control Panel</li>
                                            <li>Safe and repeatable</li>
                                            <li>Manual override capability</li>
                                            <li>High accuracy
                                                <ul class="no-style">
                                                    <li>- Average Z-distance ≤ 10mm</li>
                                                    <li>- Average X-distance ≤ 10mm</li>
                                                    <li>- Average Y-distance ≤ 10mm</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </c:if>
                                        <c:if test="${not empty pageairport5.brochure}">
                                            <div class="download">
                                                <a download href="/brochure/${pageairport5.brochure}">Download brochure</a>
                                            </div>
                                        </c:if>
                                    <div class="watch-video"><a href="#" type-video="airport_5">Watch video</a></div>
                                </div>
                            </div>
                            <div class="video" id="airport_5">
                                <video id="player_airport_5" class="video-airport_1" controls width="100%">
                                    <source src="${pageContext.request.contextPath}/media/video/airport/airport.mp4" type="video/mp4">
                                </video>
                                <div class="play-button"><span></span></div>
                                <p class="play-video" >Watch video</p>
                            </div>
                            <span class="next-detail" type-move="airport"></span>
                        </div>
                        <div class="detail-footer footer">
                            <span class="back detail-back" num-video="airport_5" type-back="airport"></span>
                            <span class="scroll-down">scroll down</span>
                        </div>
                    </div>
                </div>
                <!-- END AIRpoRT -->

            <!-- RMS -->

            <div class="item rms">
                <div class="detail-header header">
                    <a href="${pageContext.request.contextPath}/" class="logo-back-home"><img class="logo" src="${pageContext.request.contextPath}/assets/logo.png"></a>
                    <a href="#" class="menu"></a>
                </div>
                <div class="detail-content">
                    <div class="view">
                        <div class="content-left">
                            <div>
                                <img class="service-img main" src="${pageContext.request.contextPath}/assets/detail_rms.png">
                            </div>
                        </div>
                        <div class="content-right">
                            <c:if test="${empty robotic.name}">
                                <h2>Robotics Management System</h2>
                            </c:if>
                            <c:if test="${not empty robotic.name}">
                                <h2>${robotic.name}</h2>
                            </c:if>
                            <p>DESCRIPTION</p>
                            <c:if test="${empty robotic.description}">
                                <h5>Harnessing the power of technology and smart engineering, every STROBO autonomous solution is integrated with ST Engineering’s proprietary Robotics Management System (RMS) which allows seamless operation and fleet management in existing infrastructure.</h5>
                            </c:if>
                            <c:if test="${not empty robotic.description}">
                                <h5>${robotic.description}</h5>
                            </c:if>
                            <p>KEY FEATURES</p>
                            <c:if test="${not empty robotic.keyfeatures}">
                                ${robotic.keyfeatures}
                            </c:if>
                            <c:if test="${ empty robotic.keyfeatures}">
                                <ul>
                                    <li>Real-time platform status</li>
                                    <li>Fail-safe disabling</li>
                                    <li>Interface for mobility mode control</li>
                                    <li>Interface for payload control</li>
                                    <li>Manned to unmanned switching</li>
                                    <li>Linked to existing infrastructure</li>
                                    <li>Collaborative networking</li>
                                    <li>Wireless transmission Cybersecure</li>
                                </ul>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="detail-footer footer">
                    <span class="back detail-back" num-video="rms" type-back="airport"></span>
                    <span class="scroll-down">scroll down</span>
                </div>
            </div>
            <!-- END RMS -->

        </div>
        <div class="all-footers footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
    
</body>
</html>
