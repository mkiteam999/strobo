<%--
  Created by IntelliJ IDEA.
  User: mykavalli
  Date: 24/12/2018
  Time: 10:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="${pageContext.request.contextPath}/images/favicon.png" type="image/png" sizes="16x16">
    <title>News</title>

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Meta_Serif.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Lato.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/slick.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/fullpage.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery.scrollbar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/my.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/st_general.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/tablenews.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/rose.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/scrolloverflow.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/fullpage.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/slick.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/owl.carousel.js"></script>
    <script src="${pageContext.request.contextPath}/js/strobo/my.js"></script>
	<script src="${pageContext.request.contextPath}/js/strobo/rose.js"></script>
</head>
<body class="body-item news">

    <div class="menu-body">
        <img src="${pageContext.request.contextPath}/assets/menu_black_close.png" class="close-menu">
        <div class="menu-content">
            <div class="content">
                <ul>
                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#home' && menu.status == true}">
                            <li><a class="menu-item" data-menu="home" href="#home">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#about' && menu.status == true}">
                            <li><a class="menu-item" data-menu="about" href="${pageContext.request.contextPath}/#about-us">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#core' && menu.status == true}">
                            <li><a class="menu-item" data-menu="core" href="${pageContext.request.contextPath}/technology">${menu.value}</a></li>
                        </c:if>
                    </c:forEach>
                    <li><a  class="menu-item solutions" data-menu="solutions" href="${pageContext.request.contextPath}/services">Our Solutions</a>
                        <ol class="submenu">
                            <c:forEach items="${menuDefault}" var="menu">
                                <c:if test="${menu.url == '#seaport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="seaport" href="${pageContext.request.contextPath}/services/seaport">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#manufacturing' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="manufacturing" href="${pageContext.request.contextPath}/services/manufacturing">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#warehouse' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="warehouse" href="${pageContext.request.contextPath}/services/warehouse">${menu.value}</a></li>
                                </c:if>
                                <c:if test="${menu.url == '#airport' && menu.status == true}">
                                    <li><a class="sub-item" data-menu="airport" href="${pageContext.request.contextPath}/services/airport">${menu.value}</a></li>
                                </c:if>
                            </c:forEach>
                        </ol>
                    </li>

                    <c:forEach items="${menuNew}" var="menu">
                        <c:if test="${menu.status == true}">
                            <li><a class="menu-item" href="${pageContext.request.contextPath}/${menu.url}">${fn:toUpperCase(menu.value)}</a></li>
                        </c:if>
                    </c:forEach>

                    <c:forEach items="${menuDefault}" var="menu">
                        <c:if test="${menu.url == '#contact' && menu.status == true}">
                            <li><a class="menu-item" data-menu="contact" href="${pageContext.request.contextPath}/contact">${menu.value}</a></li>
                        </c:if>
                        <c:if test="${menu.url == '#news' && menu.status == true}">
                            <li><a class="menu-item" data-menu="news" href="${pageContext.request.contextPath}/news">News</a></li>
                        </c:if>
                    </c:forEach>
                    <li><img class="st-engineering" src="${pageContext.request.contextPath}/assets/ST_engineering.png"></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="body-content news">
        <div class="manufacturing-header header">
            <a href="${pageContext.request.contextPath}/" class="logo-back-home">
                <img class="logo" src="${pageContext.request.contextPath}/assets/logo.png">
            </a>
            <a href="#" class="menu"></a>
        </div>
			<div class="news-content">
				<div class="container">
					<div class="page-title"><h1>News</h1></div>
					<c:if test="${not empty listNews}">
						<c:forEach items="${listNews}" var="news" varStatus="loop">
						<input type="hidden" name= "index" value = "${loop.index}">
							<c:if test="${loop.index % 2 == 0}">
								<div class="row-item">
									<div class="col-image">
									<div class="image-wrap">
										<img src="${pageContext.request.contextPath}/assets/${news.feature_image}" alt="">
										<c:if test="${news.type == 1}"><span class="events">EVENTS</span></c:if>		
										<c:if test="${news.type == 2}"><span class="new-article">NEWS ARTICLE</span></c:if>		
										<c:if test="${news.type == 3}"><span class="press-release">PRESS RELEASE</span></c:if>		
										
									</div>
									</div>
									<div class="col-text">
										<div class="text-wrap">
											<div class="date newsdate"><fmt:formatDate value="${news.date_create}" pattern="dd-MM-yyyy"/></div>
											<h3>${news.title}</h3>
											<div class="desc"><p>${news.short_description}<p></div>
											<c:set var = "title" value = "${news.title}"/>
											<c:set var = "titleLower" value = "${fn:toLowerCase(news.title)}"/>
											<c:set var = "titleURL" value = "${fn:replace(titleLower, ' ', '-')}"/>
											<c:set var = "dateOrigin" value = "${news.date_create}"/>
											<c:set var = "dateURL" value = "${fn:replace(dateOrigin, '-', '')}"/>
											<c:set var="rand"><%= java.lang.Math.round((java.lang.Math.random()+2) * 100) %></c:set>
											<a class = "read-more" href="/news/${titleURL}-${dateURL}${news.id}${rand}">READ MORE</a>
										</div>
									</div>
								</div>
							</c:if>
							<c:if test="${loop.index % 2 == 1}">
								<div class="row-item">
									<div class="col-image">
										<div class="image-wrap">
											<img src="${pageContext.request.contextPath}/assets/${news.feature_image}" alt="">
											<c:if test="${news.type == 1}"><span class="events">EVENTS</span></c:if>		
											<c:if test="${news.type == 2}"><span class="new-article">NEWS ARTICLE</span></c:if>		
											<c:if test="${news.type == 3}"><span class="press-release">PRESS RELEASE</span></c:if>
										</div>
									</div>
									<div class="col-text">
										<div class="text-wrap">
											<div class="date newsdate"><fmt:formatDate value="${news.date_create}" pattern="dd-MM-yyyy"/></div>
											<h3>${news.title}</h3>
											<div class="desc"><p>${news.short_description}<p></div>
											<c:set var = "title" value = "${news.title}"/>
											<c:set var = "titleLower" value = "${fn:toLowerCase(news.title)}"/>
											<c:set var = "titleURL" value = "${fn:replace(titleLower, ' ', '-')}"/>
											<c:set var = "dateOrigin" value = "${news.date_create}"/>
											<c:set var = "dateURL" value = "${fn:replace(dateOrigin, '-', '')}"/>
											<c:set var="rand"><%= java.lang.Math.round((java.lang.Math.random()+2) * 100) %></c:set>
											<a class = "read-more" href="/news/${titleURL}-${dateURL}${news.id}${rand}">READ MORE</a>
										</div>
									</div>
								</div>
							</c:if>
						</c:forEach>
					</c:if>
					<div class="pagination">
						<ul class="page-numbers">
						<c:if test="${not empty lsPaging}">
							<c:forEach items="${lsPaging}" var ="page">
								<c:if test="${page != pageCurrent}">
									<li><a class="page-numbers" href="/news/page/${page}">${page}</a></li>
								</c:if>
								<c:if test="${page == pageCurrent}">
									<li><a class="page-numbers current" href="/news/page/${page}">${page}</a></li>
								</c:if>
							</c:forEach>
						</c:if>
					</ul>
					</div>
				</div>
			</div>
        <div class="all-footers footer">
            <p class="footer">COPYRIGHT @2018 ST ENGINEERING LAND SYSTEMS LTD. | ALL RIGHTS RESERVED</p>
        </div>
    </div>
</body>
</html>
