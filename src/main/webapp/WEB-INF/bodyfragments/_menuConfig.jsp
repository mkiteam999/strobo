<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${flag == true}">
	<c:if test="${success == true}">
	    <div class="alert alert-success">
	        <strong>Welldone!</strong> Update successfully!
	    </div>
	</c:if>
	
	<c:if test="${success != true}">
	    <div class="alert alert-danger">
	        <strong>Error!</strong> Update Fail!
	    </div>
	</c:if>
</c:if>

<h1 class="page-title">Menu</h1>
<div class="row grid-margin menu-config">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<label class="col-12">Menu Default</label>
					<c:forEach items="${menuDefault}" var="menu">
						<div class="col-12">
							<form method="post" enctype="multipart/form-data">
								<input type="hidden" name="id" value="${menu.id}">
								<input type="hidden" name="type" value="${menu.type}">
								<input type="hidden" name="url" value="${menu.url}">
								<div class="form-group form-inline">
									<input class="form-control" name="value" id="urlvalue${menu.id}" value="${menu.value}" readonly/>
									<div class="form-check" id="form_check${menu.id}" style="display: none;">
										<label class="form-check-label">
											<input type="checkbox" name="status" <c:if test="${menu.status == true}" > checked </c:if> class="form-check-input" >
											Show
										</label>
									</div>
									<button class="btn btn-sm btn-primary btn-save-menu" id="btn-save-menu${menu.id}" type="submit"><i class="mdi mdi-content-save"></i></button>
									<button class="btn btn-sm btn-cancel-menu" id="btn-cancel-menu${menu.id}" type="button" id-input="${menu.id}"><i class="mdi mdi-close"></i></button>
									<button class="btn btn-sm btn-edit-menu" id="btn-edit-menu${menu.id}"  type="button" id-input="${menu.id}"><i class="mdi mdi-pencil"></i></button>
								</div>
							</form>
						</div>
					</c:forEach>
					<c:if test="${not empty menuNew}">
						<label class="col-12">Menu New</label>
						<c:forEach items="${menuNew}" var="menu_n">
							<div class="col-12">
								<form method="post" enctype="multipart/form-data">
									<input type="hidden" name="id" value="${menu_n.id}">
									<input type="hidden" name="type" value="${menu_n.type}">
									<div class="form-group form-inline">
										<input class="form-control" name="value" id="urlvalue${menu_n.id}" value="${menu_n.value}" disabled/>
										<select class="form-control urlSelect" name="url" id="urlSelect${menu_n.id}" style="display: none;" >
											<option value="${menu_n.url}" selected>${menu_n.url}</option>
											<c:forEach items="${urlFrees}" var="url_Free">
												<option value="${url_Free}">${url_Free}</option>
											</c:forEach>
										</select>
										<div class="form-check" id="form_check${menu_n.id}" style="display: none;">
											<label class="form-check-label">
												<input type="checkbox" name="status" <c:if test="${menu_n.status == true}" > checked </c:if> class="form-check-input" >
												Show
											</label>
										</div>
										<button class="btn btn-sm btn-primary btn-save-menu" id="btn-save-menu${menu_n.id}" type="submit"><i class="mdi mdi-content-save"></i></button>
										<button class="btn btn-sm btn-danger btn-delete-menu" id="btn-delete-menu${menu_n.id}" type="button" id-input="${menu_n.id}"><i class="mdi mdi-delete"></i></button>
										<button class="btn btn-sm btn-cancel-menu" id="btn-cancel-menu${menu_n.id}" type="button" id-input="${menu_n.id}"><i class="mdi mdi-close"></i></button>
										<button class="btn btn-sm btn-edit-menu" id="btn-edit-menu${menu_n.id}"  type="button" id-input="${menu_n.id}"><i class="mdi mdi-pencil"></i></button>
									</div>
								</form>
							</div>
						</c:forEach>
					</c:if>
					<div class="col-12 add-menu">
						<form method="post" enctype="multipart/form-data">
							<input type="hidden" name="type" value="new">
							<div class="form-group form-inline">
								<input class="form-control" name="value" id="urlvalue" value=""/>
								<select class="form-control urlSelect" name="url" id="urlSelectx" style="display: none;" >
									<c:if test="${not empty urlFrees}">
										<c:forEach items="${urlFrees}" var="url_Free">
											<option value="${url_Free}">${url_Free}</option>
										</c:forEach>
									</c:if>
									<c:if test="${empty urlFrees}">
										<option value="#">#</option>
									</c:if>
								</select>
								<div class="form-check">
									<label class="form-check-label">
										<input type="checkbox" name="status" checked class="form-check-input" >
										Show
									</label>
								</div>
								<button class="btn btn-sm btn-primary btn-save-menu" id="btn-save-menux" type="submit"><i class="mdi mdi-content-save"></i></button>
								<button class="btn btn-sm" id="btn-cancel-menux" type="button" id-input="0"><i class="mdi mdi-close"></i></button>
							</div>
						</form>
					</div>
					<div class="col-12">
						<button class="btn btn-sm btn-success btn-add-menu" id-input="0" type="button"><i class="mdi mdi-plus"></i></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
