<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${success == true}">
    <div class="alert alert-success">
        <strong>Welldone!</strong> Update successfully!
    </div>
</c:if>

<h1 class="page-title">Profile</h1>
<form method="post" enctype="multipart/form-data">
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">Profile Information</h2>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-12">
                        	<input name="id" value="${user.id }" type="hidden"/>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="username">Username</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" name="username" id="username" class="form-control" value="${user.username }" disabled>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="email">Email</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" name="email" id="email" class="form-control" value="${user.email }">
                                    <p class="text-danger mt-2 mb-0" id="invalid_mes">Please enter correct email address.</p>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="name">Name</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" name="fullname" id="name" class="form-control" value="${user.fullname }">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="address">Address</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <textarea name="address" id="address" class="form-control" rows="4">${user.address }</textarea>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="phone">Phone</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="number" name="phone" id="phone" class="form-control" value="${user.phone }">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p class="text-center">
        <button class="btn btn-primary btn-save" type="submit">Save</button>
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/admin/profile/changePassword">Change password</a>
    </p>
</form>

