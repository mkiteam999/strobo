<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${success == true}">
	<div class="alert alert-success">
		<strong>Welldone!</strong> Update successfully!
	</div>
</c:if>
<h1 class="page-title">Home Page</h1>
<form action="/admin/home" method="post" enctype="multipart/form-data" >
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">General Information</h2>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-md-6 col-xs-12">
                                    <label for="title">Title</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="${homePage.title}" required>
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="pageName">Page Name</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" class="form-control" name="pageName" id="pageName" placeholder="Page Name" value="${homePage.pageName}">
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="meta">Meta</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <textarea type="text" class="form-control" name="meta" id="meta" placeholder="Meta" rows="6">${homePage.meta}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <%--<div class="col-12">--%>
                                <%--<div class="form-check">--%>
                                    <%--<label class="form-check-label">--%>
                                        <%--<input type="checkbox" name="status" <c:if test="${homePage.status == true}" > checked </c:if> class="form-check-input" >--%>
                                        <%--Enable--%>
                                    <%--</label>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="createdDate">Date Create</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" class="form-control" id="createdDate" placeholder="Date Create" disabled value="${homePage.createdDate}">
                                </div>
                            </div>
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="keyword">KeyWord</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <textarea type="text" class="form-control" name="keyword" id="keyword" placeholder="Keyword" rows="6">${homePage.keyword}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">Dashboard Information</h2>
                <div class="card-body">
                    <div class="row grid-margin">
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="heading1" >Home Title</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" id="heading1" name="heading1" class="form-control" value="${homePage.heading1}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <div class="form-group form-inline">
                                <div class="col-lg-3 col-sm-6 col-md-12">
                                    <label for="buttonLabel">Button label</label>
                                </div>
                                <div class="col-lg-9 col-sm-6 col-md-12">
                                    <input type="text" name="buttonLabel" id="buttonLabel" value="${homePage.buttonLabel}" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <div class="col-12">
                                <label for="multipartFile">Background video</label>
                                <input type="file" name="multipartFile" id="multipartFile" data-default-file="${pageContext.request.contextPath}/media/video/home/${homePage.file}" class="dropify form-control" accept="video/mp4">
                            </div>
<!--                             <div class="form-group form-inline"> -->
<!--                                 <div class="col-lg-3 col-sm-6 col-md-12"> -->
<!--                                     <label for="video_picker">Video Id</label> -->
<!--                                 </div> -->
<!--                                 <div class="col-lg-9 col-sm-6 col-md-12"> -->
<%--                                     <input type="text" class="form-control" id="video_picker" name="file" value="${homePage.file}"/> --%>
<!--                                 </div> -->
<!--                             </div> -->
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-12">
                            <div class="col-12">
                                <label>List video</label>
                                <c:forEach items="${listFiles}" var="file">
                                    <p>
                                        ${file.name}
                                        <span data-filename ="${file.name}" class="btn btn-sm btn-danger btn-delete fl-right" ><i class="fa fa-times"></i></span>
                                        <span data-filename ="${file.name}" class="btn btn-sm btn-primary btn-check fl-right" ><i class="fa fa-check"></i></span>
                                    </p>
                                    <hr>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p class="text-center"><button class="btn btn-primary" type="submit">Save</button></p>
</form>

<script type="text/javascript">
$(document).ready(function () {
    $('span.btn-check').click(function (e) {
        document.getElementById("video_picker").value = $(this).attr('data-filename');
    });
    $('span.btn-delete').click(function (e) {
        var filename =  $(this).attr('data-filename');
        $.ajax({
            type: 'POST',
            url: '${pageContext.request.contextPath}/admin/home/delete/'+filename,
            data: {},
            success: function(){
                window.location.href = '${pageContext.request.contextPath}/admin/home';
            },
            error: function(response){
                window.location.href = '${pageContext.request.contextPath}/admin/home/error';
            }
        });
    });
});
</script>