<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
.hide {
	display: none;
}
</style>
</head>
<body>
	<form id="formAirport" name="formAirport" action="/admin/secondlayer"
		enctype="multipart/form-data" method="post">
		<div class="row grid-margin">
			<div class="col-12">
				<div class="card">
					<h2 class="card-header">Page Airport</h2>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-sm-6 col-md-12">
								<input type="hidden" name="pagename" id="pagename" value="pageairport">
								<label for="description">Description</label>
								<textarea type="text" class="form-control" name="pagecontent" id="pagecontent" placeholder="Description" rows="8">${pageairport.pageContent}</textarea>
							</div>
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="image">Image</label> <input type="file" id="fileImageLayer2" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${pageairport.pageName}.png" name="fileImageLayer2">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p class="text-center">
			<button class="btn btn-primary" type="submit" id="savepageAirport">Save</button>
		</p>
	</form>
	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageairport1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport1" data-toggle="collapse">${pageairport1.content}</a>
					</c:if>
					<c:if test="${empty pageairport1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport1" data-toggle="collapse">Page
							Airport1</a>
					</c:if>
				</h2>
				<div id="collapsepageairport1" class="collapse">
					<form id="formImageContentAirport1" name="formImageContentAirport1"
						action="/admin/saveimagecontent" enctype="multipart/form-data"
						method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pageairport1">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageairport1.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> 
									<input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pageairport1.pageName}.png">
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageairport1.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageairport1.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
							</div>
							<div class="row grid-margin">
									
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent" id="tinyMceExample" rows="3">${pageairport1.detailContent}</textarea>
								</div>
							</div>
							
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit" id="saveformImageContentAirport1">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageairport1.listSlide}">
						<div id="hasSlideGroupAirport1" class="slideImage4 col-12">
							<c:forEach items="${pageairport1.listSlide}" var="slide">
								<form id="formslideairport1" name="formslideairport1" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slidename">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}">
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slideimage">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-airport1">
						<form id="formslide" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post" class=" col-12">
							<input type="hidden" name="formname" id="formname" value="pageairport1">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slidename">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" >
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slideimage">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-airport1">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageairport2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport2" data-toggle="collapse">${pageairport2.content}</a>
					</c:if>
					<c:if test="${empty pageairport2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport2" data-toggle="collapse">Page
							Airport2</a>
					</c:if>
				</h2>
				<div id="collapsepageairport2" class="collapse">
					<form id="formImageContentAirport2" name="formImageContentAirport2"
						action="/admin/saveimagecontent" enctype="multipart/form-data"
						method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pageairport2">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageairport2.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pageairport2.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageairport2.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageairport2.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pageairport2.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentAirport2">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageairport2.listSlide}">
						<div id="hasSlideGroupAirport2" class="slideImage4 col-12">
							<c:forEach items="${pageairport2.listSlide}" var="slide">
								<form id="formslideairport2" name="formslideairport2" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class=" col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slidename">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slideimage">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-airport2">
						<form id="formslide" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname" value="pageairport2">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slidename">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" >
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slideimage">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button" id="add-slide-airport2">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageairport3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport3" data-toggle="collapse">${pageairport3.content}</a>
					</c:if>
					<c:if test="${empty pageairport3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport3" data-toggle="collapse">Page
							Airport3</a>
					</c:if>
				</h2>
				<div id="collapsepageairport3" class="collapse">
					<form id="formImageContentAirport3" name="formImageContentAirport3"
						action="/admin/saveimagecontent" enctype="multipart/form-data"
						method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pageairport3">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageairport3.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pageairport3.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageairport3.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageairport3.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pageairport3.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentAirport3">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageairport3.listSlide}">
						<div id="hasSlideGroupAirport3" class="slideImage4 col-12">
							<c:forEach items="${pageairport3.listSlide}" var="slide">
								<form id="formslideairport3" name="formslideairport3" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slidename">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slideimage">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-airport3">
						<form id="formslide" class=" col-12" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post">
							<input type="hidden" name="formname" id="formname" value="pageairport3">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slidename">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name"  >
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slideimage">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control"  name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-airport3">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageairport4.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport4" data-toggle="collapse">${pageairport4.content}</a>
					</c:if>
					<c:if test="${empty pageairport4.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport4" data-toggle="collapse">Page
							Airport4</a>
					</c:if>
				</h2>
				<div id="collapsepageairport4" class="collapse">
					<form id="formImageContentAirport4" name="formImageContentAirport4" action="/admin/saveimagecontent" enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pageairport4">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageairport4.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pageairport4.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageairport4.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageairport4.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pageairport4.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentAirport4">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageairport4.listSlide}">
						<div id="hasSlideGroupAirport4" class="slideImage4 col-12">
							<c:forEach items="${pageairport4.listSlide}" var="slide">
								<form id="formslideairport4" name="formslideairport4" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class=" col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slideimage">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-airport4">
						<form id="formslide" class="col-12" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post">
							<input type="hidden" name="formname" id="formname" value="pageairport4">
							<div class="row grid-margin">
								<div class="col-12 grid-margin">
									<div class="row">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" >
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-airport4">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pageairport5.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport5" data-toggle="collapse">${pageairport5.content}</a>
					</c:if>
					<c:if test="${empty pageairport5.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepageairport5" data-toggle="collapse">Page
							Airport5</a>
					</c:if>
				</h2>
				<div id="collapsepageairport5" class="collapse">
					<form id="formImageContentAirport5" name="formImageContentAirport5" action="/admin/saveimagecontent" enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pageairport5">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pageairport5.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file"
										class="dropify form-control" name="fileImageLayer3"
										id="fileImageLayer3"
										data-default-file="${pageContext.request.contextPath}/media/images/${pageairport5.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pageairport5.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pageairport5.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pageairport5.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentAirport5">Save</button>
						</p>
					</form>
					<c:if test="${not empty pageairport5.listSlide}">
						<div id="hasSlideGroupAirport5" class="slideImage4 col-12">
							<c:forEach items="${pageairport5.listSlide}" var="slide">
								<form id="formslideairport5" name="formslideairport5"
									action="/admin/updateslide" enctype="multipart/form-data"
									method="POST" class=" col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname"
										value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="slideimage">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-airport5">
						<form id="formslide" name="formslide" action="/admin/saveslide"
							enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname"
								value="pageairport5">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slidename">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" >
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slideimage">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" data-default-file="" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-airport5">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var hasSlideGroupAirport1 = document
				.getElementById("hasSlideGroupAirport1");
		var hasSlideGroupAirport2 = document
				.getElementById("hasSlideGroupAirport2");
		var hasSlideGroupAirport3 = document
				.getElementById("hasSlideGroupAirport3");
		var hasSlideGroupAirport4 = document
				.getElementById("hasSlideGroupAirport4");
		var hasSlideGroupAirport5 = document
				.getElementById("hasSlideGroupAirport5");
		
		$(document).ready(function() {
			$('#add-slide-airport1').css('display','none');
			$('#add-slide-airport2').css('display','none');
			$('#add-slide-airport3').css('display','none');
			$('#add-slide-airport4').css('display','none');
			$('#add-slide-airport5').css('display','none');
		});
		function hasSlideAirport() {
			if (document.getElementById("hasslideAirport1").checked) {
				hasSlideGroupAirport1.style.display = "block";
			} else {
				hasSlideGroupAirport1.style.display = "none";
			}

			if (document.getElementById("hasslideAirport2").checked) {
				hasSlideGroupAirport2.style.display = "block";
			} else {
				hasSlideGroupAirport2.style.display = "none";
			}
			if (document.getElementById("hasslideAirport3").checked) {
				hasSlideGroupAirport3.style.display = "block";
			} else {
				hasSlideGroupAirport3.style.display = "none";
			}
			if (document.getElementById("hasslideAirport4").checked) {
				hasSlideGroupAirport4.style.display = "block";
			} else {
				hasSlideGroupAirport4.style.display = "none";
			}
			if (document.getElementById("hasslideAirport5").checked) {
				hasSlideGroupAirport5.style.display = "block";
			} else {
				hasSlideGroupAirport5.style.display = "none";
			}
		}
	</script>
</body>
</html>