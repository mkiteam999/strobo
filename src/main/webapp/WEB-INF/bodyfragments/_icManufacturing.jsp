<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style type="text/css">
.hide {
	display: none;
}
</style>
</head>
<body>
	<form id="formManufacturing" name="formManufacturing"
		action="/admin/secondlayer" enctype="multipart/form-data"
		method="post">
		<div class="row grid-margin">
			<div class="col-12">
				<div class="card">
					<h2 class="card-header">Page Manufacturing</h2>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-sm-6 col-md-12">
								<input type="hidden" name="pagename" id="pagename" value="pagemanufacturing">
								<label for="description">Description</label>
								<textarea class="form-control" name="pagecontent" id="pagecontent" placeholder="Description" rows="8">${pagemanufacturing.pageContent}</textarea>
							</div>
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="image">Image</label> <input type="file" id="fileImageLayer2" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${pagemanufacturing.pageName}.png" name="fileImageLayer2">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<p class="text-center">
			<button class="btn btn-primary" type="submit" id="savepageManufacturing">Save</button>
		</p>
	</form>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagemanufacturing1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagemanufacturing1" data-toggle="collapse">${pagemanufacturing1.content}</a>
					</c:if>
					<c:if test="${empty pagemanufacturing1.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagemanufacturing1" data-toggle="collapse">Page
							Manufacturing1</a>
					</c:if>
				</h2>
				<div id="collapsepagemanufacturing1" class="collapse">
					<form id="formImageContentManufacturing1"
						name="formImageContentManufacturing1"
						action="/admin/saveimagecontent" enctype="multipart/form-data"
						method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pagemanufacturing1">
									<input type="checkbox" name="hasslide" id="hasslideManufacturing1" checked class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pagemanufacturing1.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label> <input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pagemanufacturing1.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagemanufacturing1.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagemanufacturing1.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pagemanufacturing1.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentManufacturing1">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagemanufacturing1.listSlide}">
						<div id="hasSlideGroupManufacturing1" class="slideImage4 col-12">
							<c:forEach items="${pagemanufacturing1.listSlide}" var="slide">
								<form id="formslidemanufacturing1" name="formslidemanufacturing1" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-manufacturing1">
						<form id="formslide" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname" value="pagemanufacturing1">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row col-12">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-manufacturing1">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagemanufacturing2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagemanufacturing2" data-toggle="collapse">${pagemanufacturing2.content}</a>
					</c:if>
					<c:if test="${empty pagemanufacturing2.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagemanufacturing2" data-toggle="collapse">Page
							Manufacturing2</a>
					</c:if>
				</h2>
				<div id="collapsepagemanufacturing2" class="collapse">
					<form id="formImageContentManufacturing2" name="formImageContentManufacturing2" action="/admin/saveimagecontent" enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pagemanufacturing2">
									<input type="checkbox" name="hasslide" id="hasslideManufacturing2" checked class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pagemanufacturing2.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label>
									<input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pagemanufacturing2.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagemanufacturing2.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagemanufacturing2.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pagemanufacturing2.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentManufacturing2">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagemanufacturing2.listSlide}">
						<div id="hasSlideGroupManufacturing2" class="slideImage4 col-12">
							<c:forEach items="${pagemanufacturing2.listSlide}" var="slide">
								<form id="formslidemanufacturing2" name="formslidemanufacturing2" action="/admin/updateslide" enctype="multipart/form-data" method="POST">
									<input type="hidden" name="id" id="id" value="${slide.id}" class="col-12">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit"
													id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit"
													id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-manufacturing2">
						<form id="formslide" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname" value="pagemanufacturing2">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" data-default-file="" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-manufacturing2">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row grid-margin">
		<div class="col-12">
			<div class="card">
				<h2 class="mb-0 card-header set-accordion">
					<c:if test="${not empty pagemanufacturing3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagemanufacturing3" data-toggle="collapse">${pagemanufacturing3.content}</a>
					</c:if>
					<c:if test="${empty pagemanufacturing3.content}">
						<a class="c" data-parent="#accordion" aria-expanded="false"
							href="#collapsepagemanufacturing3" data-toggle="collapse">Page
							Manufacturing3</a>
					</c:if>
				</h2>

				<div id="collapsepagemanufacturing3" class="collapse">
					<form id="formImageContentManufacturing3" name="formImageContentManufacturing3" action="/admin/saveimagecontent" enctype="multipart/form-data" method="post">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-6 col-sm-6 col-md-12">
									<input type="hidden" name="imagename" id="imagename" value="pagemanufacturing3">
									<input type="checkbox" name="hasslide" id="hasslideManufacturing3" checked class="hide">
									<label for="title">Title</label>
									<input type="text" class="form-control" name="content" id="pagecontent" placeholder="Title" value="${pagemanufacturing3.content}">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<label for="title">Main Image</label>
									<input type="file" class="dropify form-control" name="fileImageLayer3" id="fileImageLayer3" data-default-file="${pageContext.request.contextPath}/media/images/${pagemanufacturing3.pageName}.png">
								</div>
								<div class="col-lg-6 col-sm-6 col-md-12">
									<div class="col-12">
										<label for="brochurefile">Brochure</label> <input
											type="file" name="brochurefile" id="brochurefile"
											data-default-file="${pageContext.request.contextPath}/documents/brochures/${pagemanufacturing3.brochure}" class="dropify form-control"
											accept="">
									</div>
								</div>
								<!-- 								<div class="col-lg-6 col-sm-6 col-md-12"> -->
								<!-- 									<div class="col-12"> -->
								<!-- 										<label for="backgroundvideo">Background video</label> <input -->
								<!-- 											type="file" name="backgroundvideo" id="backgroundvideo" -->
								<%-- 											data-default-file="${pageContext.request.contextPath}/media/video/${pagemanufacturing3.pageName}" class="dropify form-control" --%>
								<!-- 											accept="video/mp4"> -->
								<!-- 									</div> -->
								<!-- 								</div> -->
							</div>
							
							<div class="row grid-margin">
								<div class="col-12">
									<label for="tinyMceExample">Key Features</label>
									<textarea class="form-control" name="detailcontent"
										id="tinyMceExample" rows="3">${pagemanufacturing3.detailContent}</textarea>
								</div>
							</div>
						</div>
						<p class="text-center">
							<button class="btn btn-primary" type="submit"
								id="saveformImageContentManufacturing3">Save</button>
						</p>
					</form>
					<c:if test="${not empty pagemanufacturing3.listSlide}">
						<div id="hasSlideGroupManufacturing3" class="slideImage4 col-12">
							<c:forEach items="${pagemanufacturing3.listSlide}" var="slide">
								<form id="formslidemanufacturing3" name="formslidemanufacturing3" action="/admin/updateslide" enctype="multipart/form-data" method="POST" class="col-12">
									<input type="hidden" name="id" id="id" value="${slide.id}">
									<input type="hidden" name="formname" id="formname" value="${slide.slidepage}">
									<div class="row grid-margin">
										<div class="col-12">
											<div class="row grid-margin">
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="Slide">Slide name</label>
													<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="${slide.slidename}" >
												</div>
												<div class="col-lg-6 col-sm-6 col-md-12">
													<label for="image">Slide image</label>
													<input type="file" id="slideimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/${slide.slideimg}" name="slideimage">
												</div>
											</div>
											<p class="text-center mb-4 mt-4">
												<button class="btn btn-primary" type="submit" id="saveSlide" name="update">Update</button>
												<button class="btn btn-danger" type="submit" id="deleteSlide" name="delete">Delete</button>
											</p>
										</div>
									</div>
								</form>
							</c:forEach>
						</div>
					</c:if>
					<div class="add-slide col-12" id="add-slide-manufacturing3">
						<form id="formslide" name="formslide" action="/admin/saveslide" enctype="multipart/form-data" method="post" class="col-12">
							<input type="hidden" name="formname" id="formname" value="pagemanufacturing3">
							<div class="row grid-margin">
								<div class="col-12">
									<div class="row grid-margin">
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="slide">Slide name</label>
											<input type="text" class="form-control" name="slidename" id="slidename" placeholder="Slide Name" value="" required>
										</div>
										<div class="col-lg-6 col-sm-6 col-md-12">
											<label for="image">Slide image</label>
											<input type="file" id="slideimage" class="dropify form-control" data-default-file="" name="slideimage">
										</div>
									</div>
									<p class="text-center mb-4 mt-4">
										<button class="btn btn-primary" type="submit" id="saveSlide">Save</button>
									</p>
								</div>
							</div>
						</form>
					</div>
					<div class="col-12 mb-4">
						<button class="btn btn-sm btn-success btn-add-slide" title = "Add new slide" type="button"
							id="add-slide-manufacturing3">
							<i class="mdi mdi-plus"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		var hasSlideGroupManufacturing1 = document
				.getElementById("hasSlideGroupManufacturing1");
		var hasSlideGroupManufacturing2 = document
				.getElementById("hasSlideGroupManufacturing2");
		var hasSlideGroupManufacturing3 = document
				.getElementById("hasSlideGroupManufacturing3");
		$(document).ready(function() {
			$('#add-slide-manufacturing1').css('display', 'none');
			$('#add-slide-manufacturing2').css('display', 'none');
			$('#add-slide-manufacturing3').css('display', 'none');
		});
		function hasSlideManufacturing() {
			if (document.getElementById("hasslideManufacturing1").checked) {
				hasSlideGroupManufacturing1.style.display = "block";
			} else {
				hasSlideGroupManufacturing1.style.display = "none";
			}

			if (document.getElementById("hasslideManufacturing2").checked) {
				hasSlideGroupManufacturing2.style.display = "block";
			} else {
				hasSlideGroupManufacturing2.style.display = "none";
			}
			if (document.getElementById("hasslideManufacturing3").checked) {
				hasSlideGroupManufacturing3.style.display = "block";
			} else {
				hasSlideGroupManufacturing3.style.display = "none";
			}
		}
	</script>
</body>
</html>