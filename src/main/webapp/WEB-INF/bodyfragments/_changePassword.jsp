<%--
  Created by IntelliJ IDEA.
  User: My Kavalli
  Date: 07/09/2018
  Time: 17:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
    <%--<title>Change Password</title>--%>
<%--</head>--%>
<%--<body>--%>
<%--</body>--%>
<%--</html>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${ not empty error}">
	<div class="alert alert-danger">Invalid old password!</div>
</c:if>

<div class="row">
    <div class="card col-lg-4 mx-auto">
        <div class="card-body px-5 py-5">
            <div class="text-center mb-4"><img src="${pageContext.request.contextPath}/images/logo.png"></div>
            <h3 class="card-title text-left mb-3 text-center">Change Password</h3>
            <form method="post" validate >
            	<input name="id" value="${user.id }" type="hidden"/>
                <div class="form-group">
                    <label>Old Password *</label>
                    <input name="old_password" id="old_password" placeholder="Old Password" type="password" class="form-control p_input">
                </div>
                <div class="form-group">
                    <label>New Password *</label>
                    <input name="new_password" id="new_password" placeholder="New Password" type="password" class="form-control p_input">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-block change-pass-btn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>