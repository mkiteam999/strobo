<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<form id="formRobotic" name="formRobotic" action="/admin/saverobotic" enctype="multipart/form-data" method="post">
		<input type="hidden" name="roboticname" id="roboticname" value="roboticname">
		<div class="row grid-margin">
			<div class="col-12">
				<div class="card">
					<h2 class="card-header">Page Robotic</h2>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="name">Name</label>
								<input type="text" class="form-control" name="name" id="name" placeholder="Name" value="${robotic.name}">
								<label for="description">Description</label>
								<input type="text" class="form-control" name="description" id="description" placeholder="Description" value="${robotic.description}">
							</div>
							<div class="col-lg-6 col-sm-6 col-md-12">
								<label for="image">Image</label>
								<input type="file" id="roboticimage" class="dropify form-control" data-default-file="${pageContext.request.contextPath}/media/images/roboticname.png" name="roboticimage">
							</div>
						</div>
						<div class="row grid-margin">
							<div class="col-12">
								<label for="tinyMceExample">Key features</label>
								<textarea class="form-control" name="keyfeatures" id="tinyMceExample" rows="5">${robotic.keyfeatures}</textarea>
							</div>
						</div>
					</div>
					<p class="text-center">
						<button class="btn btn-primary" type="submit" id="saveRobotic">Save</button>
					</p>
				</div>
			</div>
		</div>
	</form>
</body>
</html>