<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${success == true}" > <div class="alert alert-success"><strong>Welldone!</strong> Update successfully!</div> </c:if>
<c:if test="${successSaveMail == false}">
    <div class="alert alert-warning">
        <strong>Sorry!</strong> Please check your email and password!
    </div>
</c:if>
<c:if test="${successSaveMail == true}" ><div class="alert alert-success"><strong>Welldone!</strong> Generate successfully!</div></c:if>
<c:if test="${restoreSuccess == true}" >
    <div class="restore-popup">
        <div class="restore-content">
            <div class="restore-body">
                <h1 class="text-success mb-2 text-center">Restore success!</h1>
                <p class="mb-1">- Admin account / password: admin/admin</p>
                <p class="mb-1">- The key login reset success, please generate key after login.</p>
                <p class="mb-1">- Please change your profile and your password after login.</p>
                <p class="text-center mb-1"><button type="button" class="btn btn-success btn-restore-ok">Ok</button></p>
            </div>
        </div>
    </div>
</c:if>
<c:if test="${restoreSuccess == false}">
    <div class="alert alert-warning">
        <strong>Sorry!</strong> Restore Fail Please Add file Backup.Zip
    </div>
</c:if>
<div class="config-popup" style="display:none;">
	<div class="popup-body">
		<div class="popup-content text-center">
            <h2 class="text-warning sending"><i class="fa fa-refresh"></i> Please wait!</h2>
            <p class="text-warning sending mb-1">Sending your email...</p>
            <h2 class="text-success sent" id="title_success"><i class="check-circle">Success!</i></h2>
            <p class="text-success sent mb-1" id="stitle_success">The system has sent you an email containing the key login!</p>
            <button type="button" class="btn btn-success btn-sendmail-ok">Ok</button>
        </div>
	</div>
</div>
<h1 class="page-title">Site Config</h1>
<form action="/admin/config/email" method="POST">
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">General Information</h2>
                <div class="card-body">
                    <div class="row grid-margin">
                        <div class="col-lg-6 col-sm-12">
                            <label for="email">Email</label>
                            <input type="text" name="emailFrom" id="email" class="form-control <c:if test="${empty config.emailFrom}">border-danger</c:if>" value="${config.emailFrom}">
                            <p class="text-danger mt-2 mb-0" id="invalid_mes" <c:if test="${empty config.emailFrom}">style="display: block;"</c:if>>Please enter correct email address.</p>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <label for="pass">Your Email Password</label>
                            <input type="password" name="passEmail" id="pass" class="form-control" value="${config.passEmail}">
                        </div>
                    </div>
                    <c:if test="${not empty config.emailFrom && not empty config.passEmail}">
                        <div class="row grid-margin">
                            <div class="col-lg-2 col-sm-12 col-md-4">
                                <button type="button" class="btn btn-primary btn-generate">Generate Key</button>
                            </div>
                            <div class="col-lg-10 col-sm-12 col-md-8">
                                <input type="text" value="${config.admin_key}" class="form-control">
                            </div>
                        </div>
                    </c:if>
                    <p class="text-center">
                        <button type="submit" class="btn btn-primary btn-save <c:if test="${empty config.emailFrom}">disabled point_e</c:if>">Save</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</form>

<%--<div class="row grid-margin">--%>
    <%--<div class="col-12">--%>
        <%--<div class="card">--%>
            <%--<div class="card-header">Backup / Restore</div>--%>
            <%--<div class="card-body">--%>
                <%--<div class="text-center mb-3">--%>
                    <%--<button type="button" class="btn btn-primary btn-backup" id="btn-backup">Backup Data</button>--%>
                    <%--<button type="button" class="btn btn-success btn-restore" id="btn-restore">Restore Data</button>--%>
                <%--</div>--%>
                <%--<div class="block-restore">--%>
                    <%--<form action="/admin/restore" method="post" enctype="multipart/form-data">--%>
                        <%--<input type="file" name="multipartFile" class="form-control dropify">--%>
                        <%--<div class="text-center mb-3 mt-3">--%>
                            <%--<button type="submit" class="btn btn-primary" id="restore">Restore</button>--%>
                        <%--</div>--%>
                    <%--</form>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</div>--%>
<%--</div>--%>

<div class="row grid-margin">
    <div class="col-12">
        <div class="card">
            <div class="card-header">Settings Google Captcha</div>
            <div class="card-body">
                <form action="/admin/captcha" method="post">
                    <div class="form-check mb-2">
                        <label class="form-check-label">
                            <input type="checkbox" name="status" id="captcha_status" <c:if test="${captcha.status == true}" > checked </c:if> class="form-check-input">
                            Enable Google Captcha
                        </label>
                    </div>
                    <c:if test="${captcha.status == true}">
                        <c:set value="block" var="style"></c:set>
                    </c:if>
                    <c:if test="${captcha.status == false}">
                        <c:set value="none" var="style"></c:set>
                    </c:if>
                    <div class="site-key" style="display: ${style};">
                        <input type="text" class="form-control mb-2" name="siteKey" value="${captcha.siteKey}" />
                    </div>
                    <div class="text-center"><button class="btn btn-primary ml-0 mr-0" type="submit" >Save</button></div>
                </form>
            </div>
        </div>
    </div>
</div>
<form action="/admin/config" method="POST">
    <div class="row grid-margin admin_contact">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">Maintenance Mode</h2>
                <div class="card-body">
                    <div class="form-check">
                        <label class="form-check-label">
                           <input type="checkbox" name="mode" <c:if test="${config.mode == true}" > checked </c:if> class="form-check-input">
                            Enable Maintenance Mode
                        </label>
                    </div>
                    <label for="tinyMceExample">Maintenance Mode Content</label>
                    <textarea class="form-control" type="text" name="content" id="tinyMceExample" rows="3">${config.content}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="row grid-margin">
        <div class="col-12">
            <div class="card">
                <h2 class="card-header">404 Page Content</h2>
                <div class="card-body">
                    <label for="error_head">Header</label>
                    <input class="form-control" type="text" name="error_head" id="error_head" value="${config.error_head}">
                    <label for="error_content">Content</label>
                    <textarea class="form-control" type="text" name="error_content" id="error_content" rows="3">${config.error_content}</textarea>
                </div>
            </div>
        </div>
    </div>
    <p class="text-center">
        <button type="submit" class="btn btn-primary">Save</button>
    </p>
</form>

<script type="text/javascript">
    $('#captcha_status').change(function(){
        if(this.checked == true) 	{
            $('.site-key').show();
        } else {
            $('.site-key').hide();
        }
    });
</script>