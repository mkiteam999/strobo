<nav class="navbar col-lg-12 col-12 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper">
        <a class="navbar-brand brand-logo" href="${pageContext.request.contextPath}/admin/home">
            <img src="${pageContext.request.contextPath}/images/logo.png" alt="">
        </a>
        <a class="navbar-brand brand-logo-mini" href="${pageContext.request.contextPath}/admin/home"><i class="mdi mdi-cube-send"></i></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
        </button>
        <div class="nav-profile">
            <%--Hi, <a class="head-name" href="${pageContext.request.contextPath}/admin/profile">Dave Mattew ${test}</a>--%>
        </div>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
    </div>
</nav>

