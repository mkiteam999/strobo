<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="ISO-8859-1"%>


<html>
<head>
    <title><tiles:getAsString name="title" /></title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/mdi/css/materialdesignicons.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/icheck/skins/all.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/dropify/dist/css/dropify.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/node_modules/font-awesome/css/font-awesome.min.css">
    <link src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"></link>
    <link src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.6.6/jquery.fullPage.css"></link>
    <link src="${pageContext.request.contextPath}/node_modules/sweetalert2/dist/sweetalert2.min.css"></link>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/custom.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/images/favicon.png" />
    <%--<script src="${pageContext.request.contextPath}/node_modules/jquery/dist/jquery.min.js"></script>--%>
    <script src="${pageContext.request.contextPath}/js/strobo/jquery-3.3.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body class="navbar-primary">
    <div class="container-scroller">
    <tiles:insertAttribute name="header" />
    <div class="container-fluid page-body-wrapper">
        <div class="row row-offcanvas row-offcanvas-right">
            <tiles:insertAttribute name="menu" />
            <div class="content-wrapper">
                <tiles:insertAttribute name="body" />
            </div>
            <tiles:insertAttribute name="footer" />
        </div>
    </div>
</div>

<!-- plugins:js -->
<script src="${pageContext.request.contextPath}/node_modules/popper.js/dist/umd/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="${pageContext.request.contextPath}/node_modules/icheck/icheck.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/dropify/dist/js/dropify.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/tinymce/tinymce.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="${pageContext.request.contextPath}/node_modules/jquery.repeater/jquery.repeater.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="${pageContext.request.contextPath}/js/off-canvas.js"></script>
<script src="${pageContext.request.contextPath}/js/hoverable-collapse.js"></script>
<script src="${pageContext.request.contextPath}/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="${pageContext.request.contextPath}/js/dropify.js"></script>
<script src="${pageContext.request.contextPath}/js/editorDemo.js"></script>
<script src="${pageContext.request.contextPath}/js/alerts.js"></script>
<script src="${pageContext.request.contextPath}/js/form-repeater.js"></script>
<script src="${pageContext.request.contextPath}/js/strobo/custom.js"></script>
<script src="${pageContext.request.contextPath}/js/strobo/admin.js"></script>
<!-- End custom js for this page-->
</body>
</html>