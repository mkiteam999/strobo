package com.aht.tb.entity;

import lombok.Data;

@Data
public class TechnologyFull {
    
    private String technologyName;
    
    private String title;

    private String heading1;

    private String description1;

    private String heading2;

    private String description2;
    
    private String feature1;

    private String feature1name;

    private String feature1image;
    
    private String feature2;
    
    private String feature2name;

    private String feature2image;
    
    private String feature3;

    private String feature3name;

    private String feature3image;
    
    private String feature4;

    private String feature4name;

    private String feature4image;
    
    private String feature5;

    private String feature5name;

    private String feature5image;
    
    private String feature6;

    private String feature6name;

    private String feature6image;
    
    private String feature7;

    private String feature7name;

    private String feature7image;
    
    private String feature8;

    private String feature8name;

    private String feature8image;

}
