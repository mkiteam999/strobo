package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Entity
@Data
@Table(name="about")

public class About implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name="aboutname")
    private String aboutname;
    
    @Column(name="title1")
    private String title1;
    
    @Column(name="title2")
    private String title2;
    
    @Column(name="heading1")
    private String heading1;
    
    @Column(name="heading2")
    private String heading2;
    
    @Column(name="visit")
    private String visit;
    
    @Column(length = 5000,name="description1")
    private String description1;
    
    @Column(length = 5000,name="description2")
    private String description2;
    
    @Column(length = 5000,name="description3")
    private String description3;
    
    @Column(length = 5000,name="description4")
    private String description4;
}
