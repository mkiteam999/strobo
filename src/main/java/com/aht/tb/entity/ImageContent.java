package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

import org.hibernate.annotations.Type;

@Entity
@Table(name ="image_content")
@Data

public class ImageContent implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    
    @Column(name ="imagename")
    private String imagename;
    
    @Column(name="content")
    private String content;
    
    @Column(length = 5000, name="detailcontent")
    private String detailcontent;
    
    @Column(length = 1000, name="brochure")
    private String brochure;
    
    @Column(name="hasslide", nullable = false, columnDefinition ="boolean default false")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean hasslide; 
    
//    @Column(name="video")
//    private String video;


    public ImageContent() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagename() {
        return imagename;
    }

    public void setImagename(String imagename) {
        this.imagename = imagename;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDetailcontent() {
        return detailcontent;
    }

    public void setDetailcontent(String detailcontent) {
        this.detailcontent = detailcontent;
    }

    public String getBrochure() {
        return brochure;
    }

    public void setBrochure(String brochure) {
        this.brochure = brochure;
    }

    public boolean isHasslide() {
        return hasslide;
    }

    public void setHasslide(boolean hasslide) {
        this.hasslide = hasslide;
    }
}
