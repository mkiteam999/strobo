package com.aht.tb.entity;

import java.awt.Image;
import java.util.List;

import lombok.Data;

@Data
public class FullThirdLayerPage {

    private String pageTitle;

    private String pageName;

    private String mainImg;
    
    private String content;
    
    private String detailContent;
    
    private List<SlideImage> listSlide;
    
    private boolean hasSlide;
    
    private String brochure;
    
    private String video;
}
