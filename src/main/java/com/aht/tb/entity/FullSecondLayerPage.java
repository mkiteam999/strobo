package com.aht.tb.entity;

import lombok.Data;

@Data
public class FullSecondLayerPage {

    private String pageTitle;

    private String pageName;

    private String mainImg;

    private String pageContent;
    
    private String p1;
    
    private String p1Content;

    private String p1Image;
    
    private String p2;

    private String p2Content;

    private String p2Image;
    
    private String p3;

    private String p3Content;

    private String p3Image;
    
    private String p4;

    private String p4Content;

    private String p4Image;
    
    private String p5;

    private String p5Content;

    private String p5Image;
    
    private String p6;

    private String p6Content;
    
    private String p6Image;
    
    private String linkvideo;

}
