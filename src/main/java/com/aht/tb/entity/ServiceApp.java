package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "service")
public class ServiceApp implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "title_service")
	private String titleService;

	@Column(name = "content_service", length = 500)
	private String contentService;

	@Column(name = "image_service", length = 1000)
	private String imageService;
	
	@Column(name="linkvideo", length = 500)
	private String linkvideo;

	@ManyToOne
	@JoinColumn(name = "page_info", nullable = false)
	private PageInfo pageInfo;

	public ServiceApp() {
		super();
	}

	public ServiceApp(Integer id, String titleService, String contentService, String imageService, PageInfo pageInfo) {
		super();
		this.id = id;
		this.titleService = titleService;
		this.contentService = contentService;
		this.imageService = imageService;
		this.pageInfo = pageInfo;
	}

	public ServiceApp(String titleService, String contentService, String imageService, PageInfo pageInfo) {
		super();
		this.titleService = titleService;
		this.contentService = contentService;
		this.imageService = imageService;
		this.pageInfo = pageInfo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitleService() {
		return titleService;
	}

	public void setTitleService(String titleService) {
		this.titleService = titleService;
	}

	public String getContentService() {
		return contentService;
	}

	public void setContentService(String contentService) {
		this.contentService = contentService;
	}

	public String getImageService() {
		return imageService;
	}

	public void setImageService(String imageService) {
		this.imageService = imageService;
	}

	public PageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}

    public String getLinkvideo() {
        return linkvideo;
    }

    public void setLinkvideo(String linkvideo) {
        this.linkvideo = linkvideo;
    }

}
