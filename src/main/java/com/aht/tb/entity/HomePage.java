package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
@Entity
@Table(name="home_page")
@Data
public class HomePage extends Page implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	@Column
	private String heading1;
	@Column
	private String heading2;
	@Column
	private String heading3;
	@Column(name="button_label")
	private String buttonLabel;
	@Column(name="button_link")
	private String buttonLink;
	@Column
	private String file;
}
