package com.aht.tb.entity;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class MyFile implements Serializable{
	private static final long serialVersionUID = 1L;
	private MultipartFile multipartFile;

	public MyFile() {
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}
}
