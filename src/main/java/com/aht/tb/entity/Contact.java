package com.aht.tb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import lombok.Data;

@Entity
@Table(name="contact")
@Data
public class Contact extends Page implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Email
	@Column
	private String email;
	
	@Column(length = 1000)
	private String phone;
	
	@Column(length = 5000)
	private String address;

	@Column
	private String facebook;

	@Column
	private String instagram;

	@Column
	private String twitter;

	@Column(length=5000)
	private String googleApi;
	
}
