package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.ServiceApp;

@Repository
public interface ServiceRepository extends JpaRepository<ServiceApp, Integer>{
}