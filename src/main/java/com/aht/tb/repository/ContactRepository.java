package com.aht.tb.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.Contact;
@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {
}
