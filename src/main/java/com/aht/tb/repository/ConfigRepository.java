package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aht.tb.entity.ConfigPage;

@Repository
public interface ConfigRepository extends JpaRepository<ConfigPage, Integer>{
	
}
