package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aht.tb.entity.SecondLayerPage;

@Repository
public interface SecondLayerPageRepository extends
        JpaRepository<SecondLayerPage, Integer> {

    SecondLayerPage findBypagename(String pagename);

    @Transactional
    @Modifying
    @Query("UPDATE SecondLayerPage s SET s.pagecontent = ?2, s.p1layer2 =?3, s.p2layer2 = ?4, s.p3layer2 = ?5, s.p4layer2 = ?6, s.p5layer2 =?7, s.p6layer2 = ?8 WHERE s.pagename = ?1")
    void updatePage2(String pagename, String pagecontent,
            String p1layer2, String p2layer2, String p3layer2, String p4layer2,
            String p5layer2, String p6layer2);
}
