package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.aht.tb.entity.ImageContent;

@Repository
public interface ImageContentRepository extends
        JpaRepository<ImageContent, Integer> {

    ImageContent findByimagename(String imagename);

    @Transactional
    @Modifying
    @Query("UPDATE ImageContent i SET i.content = ?2, i.hasslide = ?3 , i.detailcontent = ?4, i.brochure = ?5 WHERE i.imagename = ?1")
    void updateImageContent(String imagename, String content,
            boolean hasslide, String detailcontent, String brochure);
}
