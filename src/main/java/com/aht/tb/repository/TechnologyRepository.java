package com.aht.tb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.aht.tb.entity.Technology;

public interface TechnologyRepository extends
        JpaRepository<Technology, Integer> {

    Technology findBytechnologyName(String technologyName);

    @Transactional
    @Modifying
    @Query("UPDATE Technology t SET t.title = ?2, t.heading1 = ?3, t.description1 = ?4, t.heading2 = ?5, t.description2 = ?6  WHERE t.technologyName = ?1")
    void updateTechnology(String technologyName, String title, String heading1,
            String description1, String heading2, String description2);

}
