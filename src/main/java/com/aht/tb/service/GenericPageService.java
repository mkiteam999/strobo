package com.aht.tb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.GenericPage;
import com.aht.tb.repository.GenericPageRepository;

@Service
public class GenericPageService {
	@Autowired
	private GenericPageRepository genericPageRepository;
	//Get Generic Page 
	public List<GenericPage> getAllGenericPage(){
		List<GenericPage> listGenericPage = genericPageRepository.findAll();
		return listGenericPage;
	}
	//Save Generic Page
	public boolean doSave(GenericPage genericPage) {
		boolean success = false;
		try {
			genericPageRepository.save(genericPage);
			success = true;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return success;
	}
	//Delete Generic Page
	public boolean doDel(Integer id) {
		boolean success =false;
		try {
			genericPageRepository.deleteById(id);
			success= true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}
}
