package com.aht.tb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.PageInfo;
import com.aht.tb.repository.PageInfoRepository;

@Service
public class PageInfoService {
	
	@Autowired
	private PageInfoRepository pageInfoRepository;
	
	//Get all PageInfo
	public List<PageInfo> getAllPageInfo(){
		return pageInfoRepository.findAll();
	}
	
	//Get PageInfo by id
	public PageInfo getById(int id) {
		return pageInfoRepository.getOne(id);
	}
	
	
	//save PageInfo
	public boolean savePageInfo(PageInfo pageInfo) {
		try {
			pageInfoRepository.save(pageInfo);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
