package com.aht.tb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.Menu;
import com.aht.tb.repository.MenuRepository;

@Service
public class MenuService {
	@Autowired
	private MenuRepository menuRepository;

	// Get all menu
	public List<Menu> getAllMenu() {
		return menuRepository.findAll();
	}

	// Get list menu by type
	public List<Menu> getMenuByType(String type) {
		return menuRepository.findByType(type);
	}

	// Get menu by id
	public Menu getMenuById(int id) {
		return menuRepository.getOne(id);
	}

	// Save new menu
	public boolean saveMenu(Menu menu) {
		boolean success = false;
		try {
			menuRepository.save(menu);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}

	// Delete menu by id
	public boolean deleteById(int id) {
		boolean success = false;
		try {
			menuRepository.deleteById(id);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}

	// Delete menu by menu entity
	public boolean deleteByEntity(Menu menu) {
		boolean success = false;
		try {
			menuRepository.delete(menu);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}

}
