package com.aht.tb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.ServiceApp;
import com.aht.tb.repository.ServiceRepository;

@Service
public class ServiceAppService {
	@Autowired
	private ServiceRepository serviceRepository;

	// Get all service
	public List<ServiceApp> getAllService() {
		List<ServiceApp> listService = serviceRepository.findAll();
		return listService;
	}

	// Get service by id
	public ServiceApp getServiceById(int id) {
		ServiceApp service = serviceRepository.getOne(id);
		return service;
	}
	
	// find service by ID
	public ServiceApp findByID(int id){
	    Optional<ServiceApp> service = serviceRepository.findById(id);
	    return service.get();
	}

	// save Service
	public boolean saveService(ServiceApp serviceApp) {
		boolean success = false;
		try {
			serviceRepository.save(serviceApp);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}

	// Delete Service
	public boolean deleteService(int id) {
		boolean success = false;
		try {
			serviceRepository.deleteById(id);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return success;
	}

}
