package com.aht.tb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.Feature;
import com.aht.tb.repository.FeatureRepository;

@Service
public class FeatureService {

    @Autowired
    FeatureRepository featureRepository;
    
    //get FeaturebyName
    public Feature getFeatureByName(String featurename){
        Feature feature = featureRepository.findByfeaturename(featurename);
        return feature;
    }
    
    //get by ID
    public Feature getbyFeatureID(int id){
        Optional<Feature> feature = featureRepository.findById(id);
        return feature.get();
    }
    //delete by ID
    public void deleteByID(Feature feature){
        featureRepository.delete(feature);
    }
    //save Feature
    public void saveFeature(Feature feature){
        featureRepository.saveAndFlush(feature);
    }
    
    //update feature
    public void updateFeature(Feature feature){
        featureRepository.updateFeature(feature.getFeaturename(), feature.getFeatureimage());
    }
    
    //find all
    public List<Feature> findAll(){
        return featureRepository.findAll();
    }
    
}
