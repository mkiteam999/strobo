package com.aht.tb.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.ConfigPage;
import com.aht.tb.repository.ConfigRepository;

@Service
public class ConfigService {
	@Autowired
	private ConfigRepository configRepository;
	//Delkey
	public void removeKey(HttpServletRequest request) {
		Path pathFile = Paths.get(this.getFolderUpload(request) + ConfigPage.KEY_FILE);
		if (Files.exists(pathFile)) {
			try {
				Files.deleteIfExists(pathFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<ConfigPage> findAll = configRepository.findAll();
		if (findAll.size() > 0) {
			ConfigPage configPage = findAll.get(0);
			configPage.setAdmin_key(null);
			configRepository.save(configPage);
		}
	}
	// Lay link folder chua file key.txt
	public File getFolderUpload(HttpServletRequest request) {
		String uploadRootPath = request.getServletContext().getRealPath("WEB-INF"+"/classes/");
		File folderUpload = new File(uploadRootPath);
		if (!folderUpload.exists()) {
			folderUpload.mkdirs();
		}
		return folderUpload;
	}

	// Get Config
	public List<ConfigPage> findConfigPage() {
		return configRepository.findAll();
	}

	// Save Config
	public boolean saveConfigPage(ConfigPage configPage) {
		boolean success = false;
		try {
			configRepository.save(configPage);
			success = true;
		} catch (Exception e) {
			e.printStackTrace();
			success = false;
		}
		return success;
	}

	// Get Current Mode
	public boolean getCurrentMode() {
		try {
			return configRepository.findAll().get(0).isMode();
		} catch (Exception e) {
			return true;
		}
	}

	// Check Key
	public boolean checkKey(String alias,HttpServletRequest request) {
		boolean check = false;
		List<ConfigPage> listConfig = configRepository.findAll();
		// Check neu co key trong data
		if (listConfig.size() > 0) {
			// co data se kiem tra co key trong ban ghi hay k
			if (listConfig.get(0).getAdmin_key() != null) {
				// check key voi data
				check = alias.equals(listConfig.get(0).getAdmin_key());
			} else {
				check = checkKeyFile(alias,request);
			}
		} else {
			check = checkKeyFile(alias,request);
		}
		return check;
	}
	
	public boolean checkKeyFile(String alias,HttpServletRequest request) {
		Path pathFile = Paths.get(this.getFolderUpload(request) + ConfigPage.KEY_FILE);
		// neu k co se kiem tra file key.txt
		if (Files.exists(pathFile)) {
			// Read File
			List<String> listString = null;
			try {
				listString = Files.readAllLines(pathFile);
				// Check key voi key.txt
				if (alias.equals(listString.get(0))) {
					return true;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		} else {
			// Neu k co se return true
			return true;
		}
	}
}
