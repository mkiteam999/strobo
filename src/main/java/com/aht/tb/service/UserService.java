package com.aht.tb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.aht.tb.entity.User;
import com.aht.tb.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public void updateUser(User user) {
		userRepository.updateUser(user.getId(), user.getEmail(), user.getAddress(),
				user.getPhone(), user.getFullname());
	}

	public void changePassword(String newPassword) {
		userRepository.changePassword(getCurrentUserLogin().getId(), bCryptPasswordEncoder.encode(newPassword));
	}

	public User findById(int id) {
		return userRepository.findById(id);
	}

	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public User getCurrentUserLogin() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return findByUsername(auth.getName());
	}

	public boolean checkOldPassword(String oldPassword) {
		boolean matches = bCryptPasswordEncoder.matches(oldPassword, getCurrentUserLogin().getPassword());
		if (matches) {
			return true;
		}

		return false;
	}
}
