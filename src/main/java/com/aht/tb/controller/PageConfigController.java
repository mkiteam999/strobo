package com.aht.tb.controller;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aht.tb.config.GenerateKey;
import com.aht.tb.entity.ConfigPage;
import com.aht.tb.entity.GGCaptcha;
import com.aht.tb.service.ConfigService;
import com.aht.tb.service.GGCaptchaService;
import com.aht.tb.service.UserService;

@Controller
public class PageConfigController {
	@Autowired
	private ConfigService configService;
	@Autowired
	private GenerateKey generateKey;
	@Autowired
	UserService userService;

	@Autowired
	private GGCaptchaService captchaService;

	// Xu ly duong dan
	public File getFolderUpload(HttpServletRequest request) {
		String uploadRootPath = request.getServletContext().getRealPath("WEB-INF" + "/classes/");
		File folderUpload = new File(uploadRootPath);
		if (!folderUpload.exists()) {
			folderUpload.mkdirs();
		}
		return folderUpload;
	}

	// View Config
	@RequestMapping(value = { "/admin/config" }, method = RequestMethod.GET)
	public String configPage(Model model) {
		List<ConfigPage> listConfigPage = configService.findConfigPage();
		if (listConfigPage.size() > 0) {
			model.addAttribute("config", listConfigPage.get(0));
		} else {
			model.addAttribute("config", null);
		}

		if (captchaService.getAll().isEmpty()) {
			model.addAttribute("captcha", null);
		} else {
			model.addAttribute("captcha", captchaService.getCaptcha());
		}
		return "configPage";
	}

	// Update Google reCaptcha
	@RequestMapping(value = "/admin/captcha", method = RequestMethod.POST)
	public String updateGoogleCaptcha(GGCaptcha captcha) {
		captchaService.saveCaptcha(captcha);
		return "redirect:/admin/config";
	}

	// Update Config
	@RequestMapping(value = { "/admin/config" }, method = RequestMethod.POST)
	public String configPage(@ModelAttribute("config_new") ConfigPage configPage, Model model) {
		List<ConfigPage> listConfigPage = configService.findConfigPage();
		if (listConfigPage.size() > 0) {
			configPage.setId(listConfigPage.get(0).getId());
			configPage.setAdmin_key(listConfigPage.get(0).getAdmin_key());
			configPage.setEmailFrom(listConfigPage.get(0).getEmailFrom());
			configPage.setPassEmail(listConfigPage.get(0).getPassEmail());
		}
		boolean success = configService.saveConfigPage(configPage);
		List<ConfigPage> listConfig = configService.findConfigPage();
		if (listConfig.size() > 0) {
			model.addAttribute("config", listConfig.get(0));
		} else {
			model.addAttribute("config", null);
		}
		model.addAttribute("success", success);

		return "configPage";
	}

	// Save email
	@RequestMapping(value = "/admin/config/email", method = RequestMethod.POST)
	public String saveEmail(@ModelAttribute() ConfigPage configPage, Model model) {
		boolean successSaveMail = false;
		List<ConfigPage> listConfigPage = configService.findConfigPage();
		if (listConfigPage.size() > 0) {
			configPage.setId(listConfigPage.get(0).getId());
			configPage.setAdmin_key(listConfigPage.get(0).getAdmin_key());
			configPage.setContent(listConfigPage.get(0).getContent());
			configPage.setError_content(listConfigPage.get(0).getError_content());
			configPage.setError_head(listConfigPage.get(0).getError_head());
			configPage.setFooter_content(listConfigPage.get(0).getFooter_content());
			configPage.setMode(listConfigPage.get(0).isMode());
		}
		// check mail
		try {
			if (generateKey.checkMail(configPage.getEmailFrom(), configPage.getPassEmail(),
					configPage.getEmailFrom())) {
				successSaveMail = configService.saveConfigPage(configPage);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<ConfigPage> listConfig = configService.findConfigPage();
		if (listConfig.size() > 0) {
			model.addAttribute("config", listConfig.get(0));
		} else {
			model.addAttribute("config", null);
		}
		model.addAttribute("successSaveMail", successSaveMail);
		return this.configPage(model);
		/* return "redirect:/admin/config"; */
	}

	// Generic KEY
	@RequestMapping(value = { "/admin/config/key" }, method = RequestMethod.POST)
	public String generatePage(Model model, HttpServletResponse response, HttpServletRequest request) {
		// Tao key
		String key = generateKey.randomKey(24);
		boolean successMail = false;
		try {
			List<ConfigPage> listConfigPage = configService.findConfigPage();
			if (listConfigPage.size() > 0) {
				String getUrlBase = generateKey.getURLBase(request);
				successMail = generateKey.sendEmail(listConfigPage.get(0).getEmailFrom(),
						listConfigPage.get(0).getPassEmail(), userService.getCurrentUserLogin().getEmail().toString(),
						key, getUrlBase);
			} else {
				model.addAttribute("successMail", false);
				return "redirect:/admin/config";
			}
			Path path = Paths.get(this.getFolderUpload(request) + "/key.txt");
			// kiem tra su ton tai cua file
			if (!Files.exists(path)) {
				Files.createFile(path);
			}
			// Ghi vao file
			ArrayList<String> lines = new ArrayList<String>();
			lines.add(key);
			Files.write(path, lines, StandardOpenOption.WRITE);
			// Ghi vao data base
			List<ConfigPage> listConfigPage1 = configService.findConfigPage();
			if (listConfigPage1.size() > 0) {
				listConfigPage1.get(0).setAdmin_key(key);
				configService.saveConfigPage(listConfigPage1.get(0));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// View
		List<ConfigPage> listConfig = configService.findConfigPage();
		if (listConfig.size() > 0) {
			model.addAttribute("config", listConfig.get(0));
		} else {
			model.addAttribute("config", null);
		}
		model.addAttribute("successMail", successMail);

		return "configPage";
	}
}
