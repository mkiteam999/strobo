package com.aht.tb.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aht.tb.entity.About;
import com.aht.tb.entity.Feature;
import com.aht.tb.entity.Robotic;
import com.aht.tb.entity.Technology;
import com.aht.tb.entity.TechnologyFull;
import com.aht.tb.service.AboutService;
import com.aht.tb.service.FeatureService;
import com.aht.tb.service.RoboticService;
import com.aht.tb.service.TechnologyService;
import com.aht.tb.utils.Constants;
import com.aht.tb.utils.FileUtils;

@Controller
public class TechnologyRoboticController {

    private static final Logger logger = Logger
            .getLogger(TechnologyRoboticController.class);

    @Autowired
    TechnologyService technologyService;

    @Autowired
    FeatureService featureService;

    @Autowired
    RoboticService roboticService;

    @Autowired
    AboutService aboutService;

    /**
     * 
     * @param model
     * @param success
     * @param pagenamera
     * @return
     * @throws Exception
     */
    @RequestMapping(value = { "/admin/technologyRobotic" }, method = RequestMethod.GET)
    private String getTechnologyRobotic(
            Model model,
            @RequestParam(value = "success", required = false) boolean success,
            @RequestParam(value = "error", required = false) boolean error,
            @RequestParam(value = "pagenamera", required = false) String pagenamera)
            throws Exception {
        logger.debug("get technologyRobotic start");
        Technology technology = technologyService
                .getTechnologyByName(Constants.TECHNOLOGY_NAME);
        TechnologyFull technologyFull = new TechnologyFull();
        if (technology != null) {
            technologyFull.setTitle(technology.getTitle());
            technologyFull.setHeading1(technology.getHeading1());
            technologyFull.setDescription1(technology.getDescription1());
            technologyFull.setHeading2(technology.getHeading2());
            technologyFull.setDescription2(technology.getDescription2());
        }
        List<Feature> lsFeatures = featureService.findAll();
        if (!lsFeatures.isEmpty()) {
            for (Feature feature : lsFeatures) {
                String featureName = feature.getFeaturename();
                switch (featureName) {
                case Constants.FEATURE_1:
                    technologyFull.setFeature1name(feature.getFeatureimage());
                    break;
                case Constants.FEATURE_2:
                    technologyFull.setFeature2name(feature.getFeatureimage());
                    break;
                case Constants.FEATURE_3:
                    technologyFull.setFeature3name(feature.getFeatureimage());
                    break;
                case Constants.FEATURE_4:
                    technologyFull.setFeature4name(feature.getFeatureimage());
                    break;
                case Constants.FEATURE_5:
                    technologyFull.setFeature5name(feature.getFeatureimage());
                    break;
                case Constants.FEATURE_6:
                    technologyFull.setFeature6name(feature.getFeatureimage());
                    break;
                case Constants.FEATURE_7:
                    technologyFull.setFeature7name(feature.getFeatureimage());
                    break;
                case Constants.FEATURE_8:
                    technologyFull.setFeature8name(feature.getFeatureimage());
                    break;

                default:
                    break;
                }
            }
        }
        Robotic existRobotic = roboticService
                .findByroboticname(Constants.ROBOTIC_NAME);
        if (existRobotic != null) {
            model.addAttribute("robotic", existRobotic);
        }
        About about = aboutService.findByAboutName(Constants.ABOUT_NAME);
        if (about != null) {
            model.addAttribute("about", about);
        }
        model.addAttribute("success", success);
        model.addAttribute("error", error);
        if (pagenamera == null) {
            pagenamera = Constants.TECHNOLOGY_NAME;
        }
        model.addAttribute("pagenamera", pagenamera);
        model.addAttribute("lsFeatures", lsFeatures);
        model.addAttribute("technology", technologyFull);
        return "technologyRobotic";
    }

    /**
     * 
     * @param model
     * @param technology
     * @param feature1name
     * @param feature2name
     * @param feature3name
     * @param feature4name
     * @param feature5name
     * @param feature6name
     * @param feature7name
     * @param feature8name
     * @param feature1image
     * @param feature2image
     * @param feature3image
     * @param feature4image
     * @param feature5image
     * @param feature6image
     * @param feature7image
     * @param feature8image
     * @param ra
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = { "/admin/savetechnology" }, method = RequestMethod.POST)
    private String doSaveTechnology(
            Model model,
            Technology technology,
            @RequestParam(value = "feature1name", required = false) String feature1name,
            @RequestParam(value = "feature2name", required = false) String feature2name,
            @RequestParam(value = "feature3name", required = false) String feature3name,
            @RequestParam(value = "feature4name", required = false) String feature4name,
            @RequestParam(value = "feature5name", required = false) String feature5name,
            @RequestParam(value = "feature6name", required = false) String feature6name,
            @RequestParam(value = "feature7name", required = false) String feature7name,
            @RequestParam(value = "feature8name", required = false) String feature8name,
            @RequestParam(value = "feature1image", required = false) MultipartFile feature1image,
            @RequestParam(value = "feature2image", required = false) MultipartFile feature2image,
            @RequestParam(value = "feature3image", required = false) MultipartFile feature3image,
            @RequestParam(value = "feature4image", required = false) MultipartFile feature4image,
            @RequestParam(value = "feature5image", required = false) MultipartFile feature5image,
            @RequestParam(value = "feature6image", required = false) MultipartFile feature6image,
            @RequestParam(value = "feature7image", required = false) MultipartFile feature7image,
            @RequestParam(value = "feature8image", required = false) MultipartFile feature8image,
            RedirectAttributes ra, HttpServletRequest request) throws Exception {
        logger.debug("save technology start");

        String technologyName = technology.getTechnologyName();
        String title = technology.getTitle().trim();
        String heading1 = technology.getHeading1().trim();
        String description1 = technology.getDescription1().trim();
        String heading2 = technology.getHeading2().trim();
        String description2 = technology.getDescription2().trim();
        Technology newTechnology = new Technology();
        newTechnology.setTechnologyName(technologyName);
        newTechnology.setTitle(title);
        newTechnology.setHeading1(heading1);
        newTechnology.setDescription1(description1);
        newTechnology.setHeading2(heading2);
        newTechnology.setDescription2(description2);

        // check existDB
        Technology existTechnology = technologyService
                .getTechnologyByName(technologyName);
        if (existTechnology != null) {
            if (title == null || title.isEmpty()) {
                newTechnology.setTitle(existTechnology.getTitle());
            }
            if (heading1 == null || heading1.isEmpty()) {
                newTechnology.setHeading1(existTechnology.getHeading1());
            }
            if (heading2 == null || heading2.isEmpty()) {
                newTechnology.setHeading2(existTechnology.getHeading2());
            }
            if (description1 == null || description1.isEmpty()) {
                newTechnology
                        .setDescription1(existTechnology.getDescription1());
            }
            if (description2 == null || description2.isEmpty()) {
                newTechnology
                        .setDescription2(existTechnology.getDescription2());
            }
            technologyService.updateTechnology(newTechnology);
        } else {
            technologyService.saveTechnology(newTechnology);
        }
        // save feature 1
//        saveFeature(feature1name, feature1image, Constants.FEATURE_1, request);
        // save feature 2
//        saveFeature(feature2name, feature2image, Constants.FEATURE_2, request);
        // save feature 3
//        saveFeature(feature3name, feature3image, Constants.FEATURE_3, request);
        // save feature 4
//        saveFeature(feature4name, feature4image, Constants.FEATURE_4, request);
        // save feature 5
//        saveFeature(feature5name, feature5image, Constants.FEATURE_5, request);
        // save feature 6
//        saveFeature(feature6name, feature6image, Constants.FEATURE_6, request);
        // save feature 7
//        saveFeature(feature7name, feature7image, Constants.FEATURE_7, request);
        // save feature 8
//        saveFeature(feature8name, feature8image, Constants.FEATURE_8, request);
        ra.addAttribute("success", true);
        ra.addAttribute("pagenamera", technologyName);
        return "redirect:/admin/technologyRobotic";
    }

    // save robotic
    @RequestMapping(value = { "/admin/saverobotic" }, method = RequestMethod.POST)
    private String doSaveRobotic(
            Model model,
            Robotic robotic,
            HttpServletRequest request,
            RedirectAttributes ra,
            @RequestParam(value = "roboticimage", required = false) MultipartFile roboticimage)
            throws Exception {
        logger.debug("start save robotic");
        String roboticname = robotic.getRoboticname();
        String name = robotic.getName();
        String description = robotic.getDescription();
        String keyfeatures = robotic.getKeyfeatures();
        byte[] roboticimagesave = null;
        if (roboticimage != null && roboticimage.getBytes().length > 0) {
            roboticimagesave = roboticimage.getBytes();
            FileUtils.saveFolderImagesPath(request, roboticname,
                    roboticimagesave);
        }
        Robotic newRobotic = new Robotic();
        newRobotic.setRoboticname(roboticname);
        newRobotic.setName(name);
        newRobotic.setDescription(description);
        newRobotic.setKeyfeatures(keyfeatures);
        Robotic existRobotic = roboticService.findByroboticname(roboticname);
        if (existRobotic != null) {
            if (name == null || name.isEmpty()) {
                newRobotic.setName(existRobotic.getName());
            }
            if (description == null || description.isEmpty()) {
                newRobotic.setDescription(existRobotic.getDescription());
            }
            if (keyfeatures == null || keyfeatures.isEmpty()) {
                newRobotic.setKeyfeatures(existRobotic.getKeyfeatures());
            }
            roboticService.updateRobotic(newRobotic);
        } else {
            roboticService.saveRobotic(newRobotic);
        }
        ra.addAttribute("success", true);
        ra.addAttribute("pagenamera", roboticname);
        return "redirect:/admin/technologyRobotic";
    }

    @RequestMapping(value = { "/admin/saveabout" }, method = RequestMethod.POST)
    private String doSaveAbout(Model model, @ModelAttribute() About about,
            RedirectAttributes ra) {
        String aboutname = about.getAboutname();
        String title1 = about.getTitle1();
        String title2 = about.getTitle2();
        String heading1 = about.getHeading1();
        String heading2 = about.getHeading2();
        String visit = about.getVisit();
        String description1 = about.getDescription1();
        String description2 = about.getDescription2();
        String description3 = about.getDescription3();
        String description4 = about.getDescription4();
        About newAbout = new About();
        newAbout.setAboutname(aboutname);
        newAbout.setTitle1(title1);
        newAbout.setTitle2(title2);
        newAbout.setHeading1(heading1);
        newAbout.setHeading2(heading2);
        newAbout.setVisit(visit);
        newAbout.setDescription1(description1);
        newAbout.setDescription2(description2);
        newAbout.setDescription3(description3);
        newAbout.setDescription4(description4);
        // check existDB
        About existAbout = aboutService.findByAboutName(aboutname);
        if (existAbout != null) {
            if (title1 == null || title1.isEmpty()) {
                about.setTitle1(existAbout.getTitle1());
            }
            if (title2 == null || title2.isEmpty()) {
                about.setTitle2(existAbout.getTitle2());
            }
            if (heading1 == null || heading1.isEmpty()) {
                about.setHeading1(existAbout.getHeading1());
            }
            if (heading2 == null || heading2.isEmpty()) {
                about.setHeading2(existAbout.getHeading2());
            }
            if (visit == null || visit.isEmpty()) {
                about.setVisit(existAbout.getVisit());
            }
            if (description1 == null || description1.isEmpty()) {
                about.setDescription1(existAbout.getDescription1());
            }
            if (description2 == null || description2.isEmpty()) {
                about.setDescription2(existAbout.getDescription2());
            }
            if (description3 == null || description3.isEmpty()) {
                about.setDescription3(existAbout.getDescription3());
            }
            if (description4 == null || description4.isEmpty()) {
                about.setDescription4(existAbout.getDescription4());
            }
            aboutService.updateAbout(newAbout);
        } else {
            aboutService.saveAbout(about);
        }
        ra.addAttribute("success", true);
        ra.addAttribute("pagenamera", aboutname);
        return "redirect:/admin/technologyRobotic";
    }

    @RequestMapping(value = { "/admin/featureimage" }, method = RequestMethod.POST)
    private String doSaveFeature(
            Model model,
            @RequestParam(value = "featurename", required = false) String featurename,
            @RequestParam(value = "featureimage", required = false) MultipartFile featureimage,
            RedirectAttributes ra, HttpServletRequest request) {
        featurename = featurename.trim();
        logger.debug("start save feature: " + featurename);
        Feature existFeature = featureService.getFeatureByName(featurename);
        if (existFeature != null) {
            ra.addAttribute("error", true);
        } else {
            Feature newFeature = new Feature();
            newFeature.setFeaturename(featurename);
            if (featureimage != null
                    && !featureimage.getOriginalFilename().isEmpty()) {
                Date date = new Date();
                String milis = String.valueOf(date.getTime());
                String filename = milis.concat(featureimage
                        .getOriginalFilename());
                newFeature.setFeatureimage(filename);
                try {
                    FileUtils.saveFolderImagesPath(request, filename,
                            featureimage.getBytes());
                } catch (Exception e) {
                    logger.error("error save file feature:" + e.getMessage());
                }
            }
            featureService.saveFeature(newFeature);
            ra.addAttribute("success", true);
        }
        return "redirect:/admin/technologyRobotic";
    }

    /**
     * 
     * @param Model
     * @param id
     * @param featureimage
     * @param ra
     * @param request
     * @return
     */
    @RequestMapping(value = { "/admin/updatefeatureimage" }, params = "update", method = RequestMethod.POST)
    private String doUpdateFeature(
            Model Model,
            @RequestParam(value = "id", required = false) int id,
            @RequestParam(value = "featurename", required = false) String featurename,
            @RequestParam(value = "featureimage", required = false) MultipartFile featureimage,
            RedirectAttributes ra, HttpServletRequest request) {
        Feature featureUpdate = featureService.getbyFeatureID(id);
        if (featureUpdate != null) {
            if (featureimage != null
                    && !featureimage.getOriginalFilename().isEmpty()) {
                Date date = new Date();
                String milis = String.valueOf(date.getTime());
                String filename = milis.concat(featureimage
                        .getOriginalFilename());
                //delete old file
                FileUtils.deleteImageFile(request, featureUpdate.getFeatureimage());
                featureUpdate.setFeatureimage(filename);
                //save new file
                try {
                    FileUtils.saveFolderImagesPath(request, filename,
                            featureimage.getBytes());
                } catch (Exception e) {
                    logger.error("error save file feature:" + e.getMessage());
                }
            }
            //check exist feature
            if(!featurename.equals(featureUpdate.getFeaturename())){
                Feature existFeature = featureService.getFeatureByName(featurename);
                if(existFeature != null){
                    ra.addAttribute("error", true);
                    return "redirect:/admin/technologyRobotic";
                }else {
                    featureUpdate.setFeaturename(featurename);
                }
            }
            featureService.saveFeature(featureUpdate);
            ra.addAttribute("success", true);
        }
        return "redirect:/admin/technologyRobotic";
    }
    
    @RequestMapping(value = { "/admin/updatefeatureimage" }, params = "delete", method = RequestMethod.POST)
    private String doDeleteFeature(
            Model Model,
            @RequestParam(value = "id", required = false) int id,
            @RequestParam(value = "featureimage", required = false) MultipartFile featureimage,
            RedirectAttributes ra, HttpServletRequest request) {
        Feature featureDelete = featureService.getbyFeatureID(id);
        if (featureDelete != null) {
            String featureimagename = featureDelete.getFeatureimage();
            featureService.deleteByID(featureDelete);
            logger.debug("delete "+  featureDelete.getFeaturename() +" success");
            boolean isDeleteFile = FileUtils.deleteImageFile(request, featureimagename);
            if(isDeleteFile){
                logger.debug("delete file: "+ featureimagename);
            }
            ra.addAttribute("success", true);
        }
        return "redirect:/admin/technologyRobotic";
    }

    /**
     * 
     * @param featurenameUpload
     * @param featureimageupload
     * @param featureType
     * @param request
     * @throws Exception
     */
    // save Feature
    private void saveFeature(String featurenameUpload,
            MultipartFile featureimageupload, String featureType,
            HttpServletRequest request) throws Exception {
        String featureContent = null;
        if (!featurenameUpload.isEmpty()) {
            featureContent = featurenameUpload.trim();
        }
        byte[] featureimage = null;
        if (featureimageupload != null
                && featureimageupload.getBytes().length > 0) {
            featureimage = featureimageupload.getBytes();
            FileUtils.saveFolderImagesPath(request, featureType, featureimage);
        }
        Feature newFeature = new Feature();
        newFeature.setFeaturename(featureType);
        newFeature.setFeatureimage(featureimageupload.getOriginalFilename());
        // check existDB
        Feature featureExist = featureService.getFeatureByName(featureType);
        if (featureExist != null) {
            if (featureContent == null || featureContent.isEmpty()) {
                newFeature.setFeatureimage(featureExist.getFeatureimage());
            }
            featureService.updateFeature(newFeature);
        } else {
            featureService.saveFeature(newFeature);
        }
    }
}
