package com.aht.tb.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aht.tb.entity.FullSecondLayerPage;
import com.aht.tb.entity.FullThirdLayerPage;
import com.aht.tb.entity.ImageContent;
import com.aht.tb.entity.SecondLayerPage;
import com.aht.tb.entity.SlideImage;
import com.aht.tb.service.ImageContentService;
import com.aht.tb.service.SecondLayerPageService;
import com.aht.tb.service.SlideImageService;
import com.aht.tb.utils.Constants;
import com.aht.tb.utils.FileUtils;

@Controller
public class ThirdLayerController {

    private static final Logger logger = Logger
            .getLogger(ThirdLayerController.class);

    @Autowired
    SecondLayerPageService secondLayerPageService;

    @Autowired
    ImageContentService imageContentService;

    @Autowired
    SlideImageService slideImageService;

    /*
     * save image content image name is p1Layer1....
     */
    @RequestMapping(value = { "/admin/detailservice" }, method = RequestMethod.GET)
    private String getImageContent(
            Model model,
            @RequestParam(value = "success", required = false) boolean success,
            @RequestParam(value = "error", required = false) boolean error,
            @RequestParam(value = "pagename", required = false) String pagenamera)
            throws Exception {
        List<SecondLayerPage> listSecondLayerPages = secondLayerPageService
                .loadAllData();
        List<ImageContent> listImageContent = imageContentService.loadAllData();
        List<FullSecondLayerPage> listFullSecondLayerPages = new ArrayList<FullSecondLayerPage>();
        // add all data second layer
        if (!listSecondLayerPages.isEmpty()) {
            for (Iterator<SecondLayerPage> iterator = listSecondLayerPages
                    .iterator(); iterator.hasNext();) {
                SecondLayerPage secondLayerPage = (SecondLayerPage) iterator
                        .next();
                FullSecondLayerPage fullSecondLayerPage = new FullSecondLayerPage();
                fullSecondLayerPage.setPageName(secondLayerPage.getPagename());
                // String mainImg = FileUtils.encodeImage(secondLayerPage
                // .getLayer2mainimg());
                fullSecondLayerPage.setPageContent(secondLayerPage
                        .getPagecontent());
                String p1L1 = secondLayerPage.getP1layer2();
                String p2L1 = secondLayerPage.getP2layer2();
                String p3L1 = secondLayerPage.getP3layer2();
                String p4L1 = secondLayerPage.getP4layer2();
                String p5L1 = secondLayerPage.getP5layer2();
                String p6L1 = secondLayerPage.getP6layer2();
                fullSecondLayerPage.setP1(p1L1);
                fullSecondLayerPage.setP2(p2L1);
                fullSecondLayerPage.setP3(p3L1);
                fullSecondLayerPage.setP4(p4L1);
                fullSecondLayerPage.setP5(p5L1);
                fullSecondLayerPage.setP6(p6L1);
                // fullSecondLayerPage.setMainImg(mainImg);
                listFullSecondLayerPages.add(fullSecondLayerPage);
            }
        }

        // add image and content for each position
        if (!listFullSecondLayerPages.isEmpty()) {
            for (FullSecondLayerPage fullSecondLayerPage : listFullSecondLayerPages) {
                String pageLayer2Name = fullSecondLayerPage.getPageName();
                if (pageLayer2Name != null) {
                    switch (pageLayer2Name) {
                    case Constants.PAGE_AIRPORT:
                        fullSecondLayerPage = setPositionContent(
                                fullSecondLayerPage, listImageContent,
                                Constants.PAGE_AIRPORT);
                        model.addAttribute(Constants.PAGE_AIRPORT,
                                fullSecondLayerPage);
                        break;
                    case Constants.PAGE_MANUFACTURING:
                        fullSecondLayerPage = setPositionContent(
                                fullSecondLayerPage, listImageContent,
                                Constants.PAGE_MANUFACTURING);
                        model.addAttribute(Constants.PAGE_MANUFACTURING,
                                fullSecondLayerPage);
                        break;
                    case Constants.PAGE_WARE_HOUSE:
                        fullSecondLayerPage = setPositionContent(
                                fullSecondLayerPage, listImageContent,
                                Constants.PAGE_WARE_HOUSE);
                        model.addAttribute(Constants.PAGE_WARE_HOUSE,
                                fullSecondLayerPage);
                        break;
                    case Constants.PAGE_SEAPORT:
                        fullSecondLayerPage = setPositionContent(
                                fullSecondLayerPage, listImageContent,
                                Constants.PAGE_SEAPORT);
                        model.addAttribute(Constants.PAGE_SEAPORT,
                                fullSecondLayerPage);
                        break;
                    default:
                        break;
                    }
                } else {
                    logger.debug("has null page name");
                }
            }
        }
        // set content for third layer page
        if (!listImageContent.isEmpty()) {
            for (ImageContent imageContent : listImageContent) {
                FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                String imagecontentPagename = imageContent.getImagename();
                fullThirdLayerPage.setPageName(imagecontentPagename);
                fullThirdLayerPage.setPageTitle(imagecontentPagename);
                fullThirdLayerPage.setContent(imageContent.getContent());
                fullThirdLayerPage.setDetailContent(imageContent
                        .getDetailcontent());
                fullThirdLayerPage.setBrochure(imageContent.getBrochure());
//                fullThirdLayerPage.setVideo(imageContent.getVideo());
                // String mainImg = FileUtils.encodeImage(imageContent
                // .getMainimg());
                // fullThirdLayerPage.setMainImg(mainImg);
                boolean hasSlide = imageContent.isHasslide();
                fullThirdLayerPage.setHasSlide(hasSlide);
                
                // include setting display slide
                if (hasSlide) {
                    List<SlideImage> lsSlideImage = slideImageService
                            .findBySlidePage(imagecontentPagename);
                    if (!lsSlideImage.isEmpty()) {
                        fullThirdLayerPage.setListSlide(lsSlideImage);
                    }
                }
                model.addAttribute(imagecontentPagename, fullThirdLayerPage);
            }
        }
        if (pagenamera == null) {
            pagenamera = "pageAirport";
        }
        model.addAttribute("success", success);
        model.addAttribute("error", error);
        model.addAttribute("pagenamera", pagenamera);
        return "detailservice";
    }
    /**
     * 
     * @param imageContent
     * @param model
     * @param myFile
     * @param ra
     * @param request
     * @return
     * @throws Exception
     */

    @RequestMapping(value = { "/admin/saveimagecontent" }, method = RequestMethod.POST)
    private String saveImageContent(
            ImageContent imageContent,
            Model model,
            @RequestParam(value = "fileImageLayer3", required = false) MultipartFile myFile,
            @RequestParam(value = "brochurefile", required = false) MultipartFile brochurefile,
//            @RequestParam(value = "backgroundvideo", required = false) MultipartFile backgroundvideo,
//            @RequestParam(value = "slideimg1", required = false) MultipartFile fileSlide1,
//            @RequestParam(value = "slideimg2", required = false) MultipartFile fileSlide2,
//            @RequestParam(value = "slideimg3", required = false) MultipartFile fileSlide3,
//            @RequestParam(value = "slideimg4", required = false) MultipartFile fileSlide4,
            RedirectAttributes ra, HttpServletRequest request) throws Exception {
        String imageName = imageContent.getImagename();
        logger.debug("save pageImageContent " + imageName);
        String content = imageContent.getContent();
        String detailcontent = imageContent.getDetailcontent();
        //set default has slide
        boolean hasSlide = true;
        byte[] mainimg = null;
        if (myFile != null && myFile.getBytes().length > 0) {
            mainimg = myFile.getBytes();
            FileUtils.saveFolderImagesPath(request, imageName, mainimg);
        }
//        String videoname = imageName.concat(Constants.VIDEO);
//        if (backgroundvideo != null
//                && !backgroundvideo.getOriginalFilename().isEmpty()) {
//            FileUtils.saveVideo(request, videoname, backgroundvideo,Constants.EMPTY_STRING);
//        }
        String filePdf = Constants.EMPTY_STRING;
        ImageContent newImageContent = new ImageContent();
        newImageContent.setImagename(imageName);
        newImageContent.setContent(content);
        newImageContent.setDetailcontent(detailcontent);
        newImageContent.setHasslide(hasSlide);
        //rename file pdf = page name
        if(brochurefile != null && !brochurefile.getOriginalFilename().isEmpty()){
            String orgFileName =  brochurefile.getOriginalFilename();
            String[] splitFile = orgFileName.split(Pattern.quote(Constants.DOT));
            String fileType = splitFile[1];
            filePdf = imageName.concat(Constants.DOT).concat(fileType);
            newImageContent.setBrochure(filePdf);
            FileUtils.savefileBrochure(request, brochurefile, filePdf);
        }
//        newImageContent.setVideo(videoname);
        // newImageContent.setMainimg(mainimg);

        SlideImage newSlideImage = new SlideImage();
        if (hasSlide) {
            newSlideImage.setSlidename(imageName);
        }
        // check existDB
        ImageContent existIC = imageContentService
                .getImageContentbyName(imageName);
        SlideImage existSI = slideImageService.getSlideImagebyName(imageName);
        if (existIC != null) {
            // update
            if (content == null || content.isEmpty()) {
                newImageContent.setContent(existIC.getContent());
            }
            if (detailcontent == null || detailcontent.isEmpty()) {
                newImageContent.setDetailcontent(existIC.getDetailcontent());
            }
            if(filePdf.isEmpty()){
                newImageContent.setBrochure(existIC.getBrochure());
            }else {
                if(existIC.getBrochure() != null && !filePdf.equals(existIC.getBrochure())){
                    FileUtils.deleteFileBrochure(request, existIC.getBrochure());
                }
            }

            imageContentService.updateImageContent(newImageContent);
            if (existSI != null) {
                if (hasSlide) {
                    slideImageService.updateSlideImage(newSlideImage);
                }
            } else {
                if (hasSlide) {
                    slideImageService.saveSlideImage(newSlideImage);
                }
            }
        } else {
            // create new
            imageContentService.saveImageContent(newImageContent);
            if (hasSlide) {
                slideImageService.saveSlideImage(newSlideImage);
            }
        }
        ra.addAttribute("success", true);
        ra.addAttribute("pagename", imageName);
        return "redirect:/admin/detailservice";
    }
    
    /**
     * 
     * @param slideimageObject
     * @param model
     * @param formname
     * @param slidename
     * @param slideimage
     * @param ra
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = { "/admin/saveslide" }, method = RequestMethod.POST)
    private String saveSlide(
            Model model,
            @RequestParam(value = "formname", required = false) String formname,
            @RequestParam(value = "slidename", required = false) String slidename,
            @RequestParam(value = "slideimage", required = false) MultipartFile slideimage,
            RedirectAttributes ra, HttpServletRequest request) throws Exception {
        slidename = slidename.trim();
        logger.debug("save slide: " + slidename);
        //check exist DB
        SlideImage newSlideImage = new SlideImage();
        newSlideImage.setSlidename(slidename);
        newSlideImage.setSlidepage(formname);
        if (slideimage != null && !slideimage.getOriginalFilename().isEmpty()) {
            String slideimageDB = formname.concat(slideimage
                    .getOriginalFilename());
            Date date = new Date();
            slideimageDB = String.valueOf(date.getTime()).concat(slideimageDB);
            newSlideImage.setSlideimg(slideimageDB);
            FileUtils.saveFolderImages(request, slideimageDB, slideimage);
        }
        slideImageService.saveSlideImage(newSlideImage);
        ra.addAttribute("success", true);
        ra.addAttribute("pagename", formname);
        return "redirect:/admin/detailservice";
    }
    
    /**
     * update file slide
     * @param model
     * @param ra
     * @param formname
     * @param slideimage
     * @param id
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping (value="/admin/updateslide", params="update", method = RequestMethod.POST)
    private String updateSlide(Model model, RedirectAttributes ra,
            @RequestParam(value = "formname", required = false) String formname,
            @RequestParam(value = "slideimage", required = false) MultipartFile slideimage,
            @RequestParam(value = "id", required = false) int id,
            HttpServletRequest request) throws Exception {
        
        SlideImage updateSlideImage = slideImageService.getSlideImageById(id);
        if(updateSlideImage != null){
            if(slideimage != null && !slideimage.getOriginalFilename().isEmpty()){
                String slideimageDB = formname.concat(slideimage.getOriginalFilename());
                Date date = new Date();
                slideimageDB = String.valueOf(date.getTime()).concat(slideimageDB);
                //delete old file
                FileUtils.deleteImageFile(request, updateSlideImage.getSlideimg());
                //save new file
                updateSlideImage.setSlideimg(slideimageDB);
                FileUtils.saveFolderImages(request, slideimageDB, slideimage);
            }
            //save db
            slideImageService.saveSlideImage(updateSlideImage);
            ra.addAttribute("success", true);
            ra.addAttribute("pagename", formname);
        }
        return "redirect:/admin/detailservice";
    }
    
    @RequestMapping (value="/admin/updateslide", params="delete", method = RequestMethod.POST)
    private String deleteSlide(Model model, RedirectAttributes ra,
            @RequestParam(value = "formname", required = false) String formname,
            @RequestParam(value = "slideimage", required = false) MultipartFile slideimage,
            @RequestParam(value = "id", required = false) int id,
            HttpServletRequest request) throws Exception {
        
        SlideImage deleteSlide = slideImageService.getSlideImageById(id);
        if(deleteSlide != null){
            //delete old file
            FileUtils.deleteImageFile(request, deleteSlide.getSlideimg());
            //delete db
            slideImageService.deleteSlideImage(deleteSlide);
            ra.addAttribute("success", true);
            ra.addAttribute("pagename", formname);
        }
        return "redirect:/admin/detailservice";
    }
    
    /**
     * 
     * @param model
     * @param brochurename
     * @param request
     * @param response
     * @return
     */
    @RequestMapping (value ="/brochure/{brochurename}", method = RequestMethod.GET)
    private String downloadBrochures(Model model,
            @PathVariable("brochurename") String brochurename,
            HttpServletRequest request, HttpServletResponse response) {
        logger.debug("start download file: " + brochurename);
        String folderBrochures = FileUtils.getPathFilePDF(request);
        String url = folderBrochures.concat(brochurename);
        downloadFile(response, url);
        return null;
    }
    
    /**
     * 
     * @param response
     * @param url
     */
    private void downloadFile(HttpServletResponse response, String url){
        try {
            File filePDF = new File(url);
            byte[] data = org.apache.commons.io.FileUtils.readFileToByteArray(filePDF);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + filePDF.getName());
            response.setContentLength(data.length);
            InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            logger.error("download file error" + e.getMessage());
        }
    }

    /**
     * set content for image position
     * @param fullSecondLayerPage
     * @param listImageContent
     * @param pageType
     * @return
     */
    private FullSecondLayerPage setPositionContent(
            FullSecondLayerPage fullSecondLayerPage,
            List<ImageContent> listImageContent, String pageType) {
        // set position 1
        String p1 = fullSecondLayerPage.getP1();
        if (p1 != null) {
            ImageContent ic1 = listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p1)).findAny().orElse(null);
            if (ic1 != null) {
                fullSecondLayerPage.setP1Content(ic1.getContent());
                // String p1Image = FileUtils.encodeImage(ic1.getMainimg());
                // fullSecondLayerPage.setP1Image(p1Image);
            }
        }
        // set position 2
        String p2 = fullSecondLayerPage.getP2();
        if (p2 != null) {
            ImageContent ic2 = (ImageContent) listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p2)).findAny().orElse(null);
            if (ic2 != null) {
                fullSecondLayerPage.setP2Content(ic2.getContent());
                // String p2Image = FileUtils.encodeImage(ic2.getMainimg());
                // fullSecondLayerPage.setP2Image(p2Image);
            }
        }
        // set position 3
        String p3 = fullSecondLayerPage.getP3();
        if (p3 != null) {
            ImageContent ic3 = (ImageContent) listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p3)).findAny().orElse(null);
            if (ic3 != null) {
                fullSecondLayerPage.setP3Content(ic3.getContent());
                // String p3Image = FileUtils.encodeImage(ic3.getMainimg());
                // fullSecondLayerPage.setP3Image(p3Image);
            }
        }
        // only page airport and page ware house has pos4 and pos5
        if (Constants.PAGE_AIRPORT.equalsIgnoreCase(pageType)
                || Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageType)) {
            // set position 4
            String p4 = fullSecondLayerPage.getP4();
            if (p4 != null) {
                ImageContent ic4 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p4)).findAny().orElse(null);
                if (ic4 != null) {
                    fullSecondLayerPage.setP4Content(ic4.getContent());
                    // String p4Image = FileUtils.encodeImage(ic4.getMainimg());
                    // fullSecondLayerPage.setP4Image(p4Image);
                }
            }
            // set position 5
            String p5 = fullSecondLayerPage.getP5();
            if (p5 != null) {
                ImageContent ic5 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p5)).findAny().orElse(null);
                if (ic5 != null) {
                    fullSecondLayerPage.setP5Content(ic5.getContent());
                    // String p5Image = FileUtils.encodeImage(ic5.getMainimg());
                    // fullSecondLayerPage.setP5Image(p5Image);
                }
            }

        }
        // only ware house has pos 6
        if (Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageType)) {
            // set position 6
            String p6 = fullSecondLayerPage.getP6();
            if (p6 != null) {
                ImageContent ic6 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p6)).findAny().orElse(null);
                if (ic6 != null) {
                    fullSecondLayerPage.setP6Content(ic6.getContent());
                    // String p6Image = FileUtils.encodeImage(ic6.getMainimg());
                    // fullSecondLayerPage.setP6Image(p6Image);
                }
            }
        }
        return fullSecondLayerPage;
    }
}
