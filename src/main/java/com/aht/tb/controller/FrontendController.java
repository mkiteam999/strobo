package com.aht.tb.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aht.tb.entity.About;
import com.aht.tb.entity.Contact;
import com.aht.tb.entity.Feature;
import com.aht.tb.entity.FullSecondLayerPage;
import com.aht.tb.entity.FullThirdLayerPage;
import com.aht.tb.entity.GenericPage;
import com.aht.tb.entity.HomePage;
import com.aht.tb.entity.ImageContent;
import com.aht.tb.entity.Robotic;
import com.aht.tb.entity.SecondLayerPage;
import com.aht.tb.entity.ServiceApp;
import com.aht.tb.entity.SlideImage;
import com.aht.tb.entity.Technology;
import com.aht.tb.entity.TechnologyFull;
import com.aht.tb.repository.PageInfoRepository;
import com.aht.tb.service.AboutService;
import com.aht.tb.service.ConfigService;
import com.aht.tb.service.ContactService;
import com.aht.tb.service.FeatureService;
import com.aht.tb.service.GenericPageService;
import com.aht.tb.service.HomePageService;
import com.aht.tb.service.ImageContentService;
import com.aht.tb.service.MenuService;
import com.aht.tb.service.PageInfoService;
import com.aht.tb.service.RoboticService;
import com.aht.tb.service.SecondLayerPageService;
import com.aht.tb.service.ServiceAppService;
import com.aht.tb.service.SlideImageService;
import com.aht.tb.service.TechnologyService;
import com.aht.tb.utils.Constants;

@Controller
public class FrontendController {
    @Autowired
    private ContactService contactService;

    @Autowired
    private HomePageService homePageService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private ServiceAppService serviceAppService;

    @Autowired
    private PageInfoRepository pageInfoRepository;

    @Autowired
    private PageInfoService pageInfo;

    @Autowired
    private GenericPageService genericPageService;

    @Autowired
    private MenuService menuService;

    @Autowired
    SecondLayerPageService secondLayerPageService;

    @Autowired
    ImageContentService imageContentService;

    @Autowired
    SlideImageService slideImageService;

    @Autowired
    TechnologyService technologyService;

    @Autowired
    FeatureService featureService;
    
    @Autowired
    RoboticService roboticService;
    
    @Autowired
    AboutService aboutService;

    private static final Logger logger = Logger
            .getLogger(FrontendController.class);

    public static final int HOME_STATUS = 1;
    public static final int SERVICE_STATUS = 2;
    public static final int CONTACT_STATUS = 3;

    private boolean checkEnablePage(int statusPage) {
        if (statusPage == HOME_STATUS) {
            return homePageService.findHomePage().get(0).isStatus();
        }

        if (statusPage == SERVICE_STATUS) {
            return pageInfoRepository.findAll().get(0).isStatus();
        }

        if (statusPage == CONTACT_STATUS) {
            return contactService.findContact().get(0).isStatus();
        }

        return true;
    }

    private boolean checkCurrentMode() {
        return configService.getCurrentMode();
    }

//    @RequestMapping(value = "/news", method = RequestMethod.GET)
//    public String getNewpage(Model model) {
//        if (checkCurrentMode() == false && checkEnablePage(CONTACT_STATUS)) {
//            List<Contact> listContact = contactService.findContact();
//            if (listContact.size() > 0) {
//                model.addAttribute("contact", listContact.get(0));
//            } else {
//                model.addAttribute("contact", null);
//            }
//            model.addAttribute("menuDefault", menuService.getMenuByType("default"));
//            model.addAttribute("menuNew", menuService.getMenuByType("new"));
//            return "tintuc";
//        }
//        model.addAttribute("maintain", configService.findConfigPage().get(0));
//        return "maintaince";

//    }
    
    

    // View Index
    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        if (checkCurrentMode() == false) {
            List<HomePage> listHomePage = homePageService.findHomePage();
            if (listHomePage.size() > 0) {
                HomePage firstHomePage = listHomePage.get(0);
                // String label = firstHomePage.getButtonLabel();
                // if(!StringUtils.equalsIgnoreCase(Constants.WATCH_VIDEO,
                // label)){
                // firstHomePage.setButtonLabel(Constants.WATCH_VIDEO);
                // }
                model.addAttribute("homePage", firstHomePage);
            } else {
                model.addAttribute("homePage", null);
            }
            // edit homepage attribute
            model.addAttribute("menuDefault", menuService.getMenuByType("default"));
            model.addAttribute("menuNew", menuService.getMenuByType("new"));
            model.addAttribute("services", serviceAppService.getAllService());
            model.addAttribute("pageInfo", pageInfo.getById(1));
            model.addAttribute("homeStatus", checkEnablePage(HOME_STATUS));
            model.addAttribute("serviceStatus", checkEnablePage(SERVICE_STATUS));
            model.addAttribute("contactStatus", checkEnablePage(CONTACT_STATUS));
            List<Contact> listContact = contactService.findContact();
            if (listContact.size() > 0) {
                model.addAttribute("contacts", listContact.get(0));
            } else {
                model.addAttribute("contacts", null);
            }
            
            //get link video of 4 main pages
            String videoAirport = null;
            String videoSeaport = null;
            String videoWarehouse = null;
            String videoManufacturing = null;
            List<ServiceApp> listService = serviceAppService.getAllService();
            for (ServiceApp serviceApp : listService) {
                int id = serviceApp.getId();
                switch (id) {
                case 1:
                    //1 is seaport
                    videoSeaport = serviceApp.getLinkvideo();
                    model.addAttribute(Constants.PAGE_SEAPORT,videoSeaport);
                    break;
                case 2:
                    //2 is manufacturing
                    videoManufacturing = serviceApp.getLinkvideo();
                    model.addAttribute(Constants.PAGE_MANUFACTURING,videoManufacturing);
                    break;
                case 3:
                    //3 is warehouse
                    videoWarehouse = serviceApp.getLinkvideo();
                    model.addAttribute(Constants.PAGE_WARE_HOUSE, videoWarehouse);
                    break;
                case 4:
                    //4 is airport
                    videoAirport = serviceApp.getLinkvideo();
                    model.addAttribute(Constants.PAGE_AIRPORT, videoAirport);
                    break;

                default:
                    break;
                }
            }

            Robotic robotic = roboticService.findByroboticname(Constants.ROBOTIC_NAME);
            if(robotic != null){
                model.addAttribute("robotic", robotic);
            }
            About about = aboutService.findByAboutName(Constants.ABOUT_NAME);
            if(about != null){
                model.addAttribute("about", about);
            }
            return "index";
        }
        model.addAttribute("maintain", configService.findConfigPage().get(0));
        return "maintaince";

    }
    
    /**
     * 
     * @param pagename
     * @param model
     * @return
     */
    @RequestMapping(value ="/services/{pagename}", method = RequestMethod.GET)
    public String getServicesPage(@PathVariable("pagename") String pagename,Model model){
        logger.debug("start get data of page: "+ pagename);
        String pageService = Constants.EMPTY_STRING;
        int serviceID = 1;
        if(!pagename.isEmpty()){
            switch (pagename) {
            case Constants.AIRPORT:
                serviceID = 4;
                pageService = Constants.PAGE_AIRPORT;
                break;
            case Constants.SEAPORT:
                serviceID = 1;
                pageService = Constants.PAGE_SEAPORT;
                break;
            case Constants.MANUFACTURING:
                serviceID = 2;
                pageService = Constants.PAGE_MANUFACTURING;
                break;
            case Constants.WARE_HOUSE:
                serviceID = 3;
                pageService = Constants.PAGE_WARE_HOUSE;
                break;

            default:
                break;
            }
        }
        //query data
        if(!pageService.isEmpty()){
            SecondLayerPage secondLayerPage = secondLayerPageService.getSecondPagebyName(pageService); 
            if(secondLayerPage != null){
                FullSecondLayerPage fullSecondLayerPage = new FullSecondLayerPage();
                ServiceApp serviceApp = serviceAppService.findByID(serviceID);
                
                fullSecondLayerPage.setPageName(secondLayerPage
                        .getPagename());
                fullSecondLayerPage.setPageContent(secondLayerPage
                        .getPagecontent());
                if(serviceApp != null){
                    fullSecondLayerPage.setLinkvideo(serviceApp.getLinkvideo());
                }
                String p1L1 = secondLayerPage.getP1layer2();
                String p2L1 = secondLayerPage.getP2layer2();
                String p3L1 = secondLayerPage.getP3layer2();
                String p4L1 = secondLayerPage.getP4layer2();
                String p5L1 = secondLayerPage.getP5layer2();
                String p6L1 = secondLayerPage.getP6layer2();
                fullSecondLayerPage.setP1(p1L1);
                fullSecondLayerPage.setP2(p2L1);
                fullSecondLayerPage.setP3(p3L1);
                fullSecondLayerPage.setP4(p4L1);
                fullSecondLayerPage.setP5(p5L1);
                fullSecondLayerPage.setP6(p6L1);
                model.addAttribute(pageService,fullSecondLayerPage);
                //set data p1L1
                if(p1L1 != null){
                    ImageContent imageContent = imageContentService.getImageContentbyName(p1L1);
                    if(imageContent != null){
                        FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                        fullThirdLayerPage = setDetail(fullThirdLayerPage, imageContent);
                        model.addAttribute(imageContent.getImagename(), fullThirdLayerPage);
                    }
                }
                //set data p2L1
                if(p2L1 != null){
                    ImageContent imageContent = imageContentService.getImageContentbyName(p2L1);
                    if(imageContent != null){
                        FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                        fullThirdLayerPage = setDetail(fullThirdLayerPage, imageContent);
                        model.addAttribute(imageContent.getImagename(), fullThirdLayerPage);
                    }
                }
                //set data p3L1
                if(p3L1 != null){
                    ImageContent imageContent = imageContentService.getImageContentbyName(p3L1);
                    if(imageContent != null){
                        FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                        fullThirdLayerPage = setDetail(fullThirdLayerPage, imageContent);
                        model.addAttribute(imageContent.getImagename(), fullThirdLayerPage);
                    }
                }
              //set data p4L1
                if(p4L1 != null){
                    ImageContent imageContent = imageContentService.getImageContentbyName(p4L1);
                    if(imageContent != null){
                        FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                        fullThirdLayerPage = setDetail(fullThirdLayerPage, imageContent);
                        model.addAttribute(imageContent.getImagename(), fullThirdLayerPage);
                    }
                }
                //set data p5L1
                if(p5L1 != null){
                    ImageContent imageContent = imageContentService.getImageContentbyName(p5L1);
                    if(imageContent != null){
                        FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                        fullThirdLayerPage= setDetail(fullThirdLayerPage, imageContent);
                        model.addAttribute(imageContent.getImagename(), fullThirdLayerPage);
                    }
                }
                //set data p6L1
                if(p6L1 != null){
                    ImageContent imageContent = imageContentService.getImageContentbyName(p6L1);
                    if(imageContent != null){
                        FullThirdLayerPage fullThirdLayerPage = new FullThirdLayerPage();
                        fullThirdLayerPage = setDetail(fullThirdLayerPage, imageContent);
                        model.addAttribute(imageContent.getImagename(), fullThirdLayerPage);
                    }
                }
            }
        }
        model.addAttribute("menuDefault", menuService.getMenuByType("default"));
        model.addAttribute("menuNew", menuService.getMenuByType("new"));
        return pagename;
    }
    
    //set data for detail
    private FullThirdLayerPage setDetail(FullThirdLayerPage fullThirdLayerPage, ImageContent imageContent){
        fullThirdLayerPage.setPageName(imageContent.getImagename());
        fullThirdLayerPage.setContent(imageContent.getContent());
        fullThirdLayerPage.setDetailContent(imageContent
                .getDetailcontent());
        fullThirdLayerPage.setBrochure(imageContent.getBrochure());
        boolean hasSlide = imageContent.isHasslide();
        fullThirdLayerPage.setHasSlide(hasSlide);
        if (hasSlide) {
            List<SlideImage> lsSlideImage = slideImageService
                    .findBySlidePage(imageContent.getImagename());
            if (!lsSlideImage.isEmpty()) {
                fullThirdLayerPage.setListSlide(lsSlideImage);
            }
        }
        return fullThirdLayerPage;
    }
    
    /**
     * 
     * @param model
     * @return
     */
    @RequestMapping(value = "/aboutus", method = RequestMethod.GET)
    public String getAboutus(Model model) {
        About about = aboutService.findByAboutName(Constants.ABOUT_NAME);
        if (about != null) {
            model.addAttribute("about", about);
        }
        model.addAttribute("menuDefault", menuService.getMenuByType("default"));
        model.addAttribute("menuNew", menuService.getMenuByType("new"));
        return "about";
    }

    @RequestMapping(value = "/services", method = RequestMethod.GET)
    public String service(Model model) {
        if (checkCurrentMode() == false && checkEnablePage(SERVICE_STATUS)) {
            model.addAttribute("menuDefault", menuService.getMenuByType("default"));
            model.addAttribute("menuNew", menuService.getMenuByType("new"));
            model.addAttribute("services", serviceAppService.getAllService());
            return "service";
        }
        model.addAttribute("maintain", configService.findConfigPage().get(0));
        return "maintaince";

    }

    // View Contact
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact(Model model) {
        if (checkCurrentMode() == false && checkEnablePage(CONTACT_STATUS)) {
            List<Contact> listContact = contactService.findContact();
            if (listContact.size() > 0) {
                model.addAttribute("contact", listContact.get(0));
            } else {
                model.addAttribute("contact", null);
            }
            model.addAttribute("menuDefault", menuService.getMenuByType("default"));
            model.addAttribute("menuNew", menuService.getMenuByType("new"));
            return "contact";
        }
        model.addAttribute("maintain", configService.findConfigPage().get(0));
        return "maintaince";

    }

    @RequestMapping(value = "/technology", method = RequestMethod.GET)
    public String getTechnology(Model model){
        Technology technology = technologyService
                .getTechnologyByName("technologyName");
        TechnologyFull technologyFull = new TechnologyFull();
        if (technology != null) {
            technologyFull.setTitle(technology.getTitle());
            technologyFull.setHeading1(technology.getHeading1());
            technologyFull.setDescription1(technology.getDescription1());
            technologyFull.setHeading2(technology.getHeading2());
            technologyFull.setDescription2(technology.getDescription2());
        }
        List<Feature> lsFeatures = featureService.findAll();
        model.addAttribute("technology", technologyFull);
        model.addAttribute("lsFeatures", lsFeatures);
        model.addAttribute("menuDefault", menuService.getMenuByType("default"));
        model.addAttribute("menuNew", menuService.getMenuByType("new"));
        return "technology";
    }
    // View Generic Page
    @RequestMapping(value = "/{alias}", method = RequestMethod.GET)
    public String doGenericPage(@PathVariable("alias") String alias, Model model) {
        // Check maintane
        String page = "error";
        List<GenericPage> list = genericPageService.getAllGenericPage();
        // Kiem tra danh sach trang tao co rong khong
        if (list.size() > 0) {
            for (GenericPage genericPage : list) {
                // kiem tra xem alias co trong db k
                if (alias.equals(genericPage.getAlias())) {
                    // alias co trong db se view
                    if (checkCurrentMode() == false && genericPage.isStatus()) {
                        List<HomePage> listHomePage = homePageService
                                .findHomePage();
                        if (listHomePage.size() > 0) {
                            model.addAttribute("homePage", listHomePage.get(0));
                        } else {
                            model.addAttribute("homePage", null);
                        }
                        model.addAttribute("genericPage", genericPage);
                        model.addAttribute("menuDefault",
                                menuService.getMenuByType("default"));
                        model.addAttribute("menuNew",
                                menuService.getMenuByType("new"));
                        model.addAttribute("services",
                                serviceAppService.getAllService());
                        model.addAttribute("pageInfo", pageInfo.getById(1));
                        page = "custompage";
                    } else {
                        model.addAttribute("maintain", configService
                                .findConfigPage().get(0));
                        return "maintaince";
                    }
                }
            }
        } else {
            page = "error";
        }
        return page;
    }

    // set content for image position
    private FullSecondLayerPage setPositionContent(
            FullSecondLayerPage fullSecondLayerPage,
            List<ImageContent> listImageContent, String pageType) {
        // set position 1
        String p1 = fullSecondLayerPage.getP1();
        if (p1 != null) {
            ImageContent ic1 = listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p1)).findAny().orElse(null);
            if (ic1 != null) {
                fullSecondLayerPage.setP1Content(ic1.getContent());
                // String p1Image = FileUtils.encodeImage(ic1.getMainimg());
                // fullSecondLayerPage.setP1Image(p1Image);
            }
        }
        // set position 2
        String p2 = fullSecondLayerPage.getP2();
        if (p2 != null) {
            ImageContent ic2 = (ImageContent) listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p2)).findAny().orElse(null);
            if (ic2 != null) {
                fullSecondLayerPage.setP2Content(ic2.getContent());
                // String p2Image = FileUtils.encodeImage(ic2.getMainimg());
                // fullSecondLayerPage.setP2Image(p2Image);
            }
        }
        // set position 3
        String p3 = fullSecondLayerPage.getP3();
        if (p3 != null) {
            ImageContent ic3 = (ImageContent) listImageContent
                    .stream()
                    .filter(ImageContent -> ImageContent.getImagename().equals(
                            p3)).findAny().orElse(null);
            if (ic3 != null) {
                fullSecondLayerPage.setP3Content(ic3.getContent());
                // String p3Image = FileUtils.encodeImage(ic3.getMainimg());
                // fullSecondLayerPage.setP3Image(p3Image);
            }
        }
        // only page airport and page ware house has pos4 and pos5
        if (Constants.PAGE_AIRPORT.equalsIgnoreCase(pageType)
                || Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageType)) {
            // set position 4
            String p4 = fullSecondLayerPage.getP4();
            if (p4 != null) {
                ImageContent ic4 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p4)).findAny().orElse(null);
                if (ic4 != null) {
                    fullSecondLayerPage.setP4Content(ic4.getContent());
                    // String p4Image = FileUtils.encodeImage(ic4.getMainimg());
                    // fullSecondLayerPage.setP4Image(p4Image);
                }
            }
            // set position 5
            String p5 = fullSecondLayerPage.getP5();
            if (p5 != null) {
                ImageContent ic5 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p5)).findAny().orElse(null);
                if (ic5 != null) {
                    fullSecondLayerPage.setP5Content(ic5.getContent());
                    // String p5Image = FileUtils.encodeImage(ic5.getMainimg());
                    // fullSecondLayerPage.setP5Image(p5Image);
                }
            }

        }
        // only ware house has pos 6
        if (Constants.PAGE_WARE_HOUSE.equalsIgnoreCase(pageType)) {
            // set position 6
            String p6 = fullSecondLayerPage.getP6();
            if (p6 != null) {
                ImageContent ic6 = (ImageContent) listImageContent
                        .stream()
                        .filter(ImageContent -> ImageContent.getImagename()
                                .equals(p6)).findAny().orElse(null);
                if (ic6 != null) {
                    fullSecondLayerPage.setP6Content(ic6.getContent());
                    // String p6Image = FileUtils.encodeImage(ic6.getMainimg());
                    // fullSecondLayerPage.setP6Image(p6Image);
                }
            }
        }
        return fullSecondLayerPage;
    }
}
