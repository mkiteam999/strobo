package com.aht.tb.controller;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.aht.tb.entity.*;
import com.aht.tb.service.*;
import com.aht.tb.utils.Constants;
import com.aht.tb.utils.DateUtils;
import com.aht.tb.utils.FileUtils;

@Controller
public class ServiceAppController {

    @Autowired
    private ServiceAppService serviceAppService;

    @Autowired
    private PageInfoService pageInfoService;

    public boolean success = false;

    private static final Logger logger = Logger
            .getLogger(ServiceAppController.class);

    // Xu ly duong dan
    public File getFolderUpload(HttpServletRequest request) {

        String uploadRootPath = request.getServletContext()
                .getRealPath("media")
                + File.separator
                + "images"
                + File.separator + "img_service";

        File folderUpload = new File(uploadRootPath);
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }

    // get all service
    @RequestMapping(value = "/admin/services", method = RequestMethod.GET)
    private String servicePage(Model model, HttpServletRequest request) {
        List<ServiceApp> listService = serviceAppService.getAllService();
        PageInfo pageInfo = pageInfoService.getAllPageInfo().get(0);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("listService", listService);
        model.addAttribute("success", success);
        model.addAttribute("uploadRootPath", getFolderUpload(request)
                .getAbsolutePath());
        success = false;
        return "servicePage";
    }

    // Update PageInfo
    @RequestMapping(value = "/admin/service/pageinfo", method = RequestMethod.POST)
    private String doSavePageInfo(PageInfo p) {
        List<PageInfo> allPageInfo = pageInfoService.getAllPageInfo();
        if (allPageInfo.size() == 0) {
            p.setCreatedDate(DateUtils.getCurrentDate());
            success = pageInfoService.savePageInfo(p);
        } else {
            PageInfo page = allPageInfo.get(0);
            if (page.getCreatedDate() == null || page.getCreatedDate() == "") {
                p.setCreatedDate(DateUtils.getCurrentDate());
            } else {
                p.setCreatedDate(page.getCreatedDate());
            }
            p.setId(page.getId());
            success = pageInfoService.savePageInfo(p);
        }

        return "redirect:/admin/services";
    }

    // Service
    @RequestMapping(value = "/admin/services", method = RequestMethod.POST)
    private String doUpdateSericeApp(
            @RequestParam("id") int id,
            @ModelAttribute() ServiceApp s1,
            @ModelAttribute("multipartFile") MyFile myFile,
            @RequestParam(value = "backgroundvideo", required = false) MultipartFile videoFile,
            HttpServletRequest request) {
        // Upload File
        MultipartFile multipartFile = myFile.getMultipartFile();
        String fileName = multipartFile.getOriginalFilename();
        try {
            File file = new File(this.getFolderUpload(request), fileName);
            multipartFile.transferTo(file);
        } catch (Exception e) {
            logger.error("upload file image fail!" + e.getMessage());
        }
        String linkVideo = Constants.EMPTY_STRING;
        if (videoFile != null && !videoFile.getOriginalFilename().isEmpty()) {
            String videoname = Constants.SEAPORT;
            switch (id) {
            case 1:
                videoname = Constants.SEAPORT;
                linkVideo = Constants.SEAPORT.concat(Constants.FILE_MP4);
                break;
                
            case 2:
                videoname = Constants.MANUFACTURING;
                linkVideo = Constants.MANUFACTURING.concat(Constants.FILE_MP4);
                break;

            case 3:
                videoname = Constants.WARE_HOUSE;
                linkVideo = Constants.WARE_HOUSE.concat(Constants.FILE_MP4);
                break;

            case 4:
                videoname = Constants.AIRPORT;
                linkVideo = Constants.AIRPORT.concat(Constants.FILE_MP4);
                break;

            default:
                break;
            }
            FileUtils.saveVideo(request, videoname, videoFile, videoname);
        }
        s1.setLinkvideo(linkVideo);
        // Set PageInfo
        List<PageInfo> allPageInfo = pageInfoService.getAllPageInfo();
        if (allPageInfo.size() > 0) {
            s1.setPageInfo(allPageInfo.get(0));
        } else {
            s1.setPageInfo(null);
        }

        // if (id != 0) {
        // update service
        if (s1.getImageService() == null) {
            if (fileName.length() > 0) {
                s1.setImageService(fileName);
            } else {
                s1.setImageService(serviceAppService.getServiceById(s1.getId())
                        .getImageService());
            }
        }
        success = serviceAppService.saveService(s1);
        /*
         * } else { // add new service List<Integer> i = new ArrayList<>(); for
         * (ServiceApp s : serviceAppService.getAllService()) {
         * i.add(s.getNumberService()); } s1.setNumberService(Collections.max(i)
         * + 1); s1.setImageService(fileName); success =
         * serviceAppService.saveService(s1); }
         */

        return "redirect:/admin/services";
    }

    // Delete one ServiceApp
    /*
     * @RequestMapping(value = "/admin/service/delete/{id}", method =
     * RequestMethod.POST) private String getOneService(@PathVariable("id") int
     * id) { success = serviceAppService.deleteService(id); return
     * "redirect:/admin/service"; }
     */

}
