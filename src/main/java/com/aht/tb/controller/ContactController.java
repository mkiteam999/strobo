package com.aht.tb.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aht.tb.entity.Contact;
import com.aht.tb.service.ContactService;

@Controller
public class ContactController {
	@Autowired
	private ContactService contactService;
	@RequestMapping(value="/admin/contactus",method = RequestMethod.GET)
	private String contactusPage(Model model) {
		List<Contact> listContact = contactService.findContact();
		if(listContact.size()>0) {
			model.addAttribute("contact",listContact.get(0));
		}else {
		model.addAttribute("contact", null);
		}
		return "contactusPage";
	}
	@RequestMapping(value="/admin/contactus", method=RequestMethod.POST)
	private String doSave(/*@ModelAttribute("contact_new")*/ Contact contact, Model model) {
		List<Contact> listContact = contactService.findContact();
		if(listContact.size()>0) {
			contact.setId(listContact.get(0).getId());
			if(listContact.get(0).getCreatedDate()==null|| listContact.get(0).getCreatedDate()=="") {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				String day = dateFormat.format(date);
				contact.setCreatedDate(day);
			}else {
				contact.setCreatedDate(listContact.get(0).getCreatedDate());
			}
		}else {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String day = dateFormat.format(date);
			contact.setCreatedDate(day);
		}
		boolean success = contactService.saveContact(contact);
		List<Contact> listContact1 = contactService.findContact();
		if(listContact1.size()>0) {
			model.addAttribute("contact",listContact1.get(0));
		}else{
			model.addAttribute("contact", null);
		}
		model.addAttribute("success",success);
		return "contactusPage";
	}
}
