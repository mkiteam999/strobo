package com.aht.tb.controller;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.aht.tb.entity.NewsEvent;
import com.aht.tb.service.EventService;
import com.aht.tb.service.MenuService;
import com.aht.tb.utils.Constants;
import com.aht.tb.utils.FileUtils;

@Controller
public class EventController {

	@Autowired
	EventService eventService;
	
	@Autowired
	MenuService menuService;

	
	@RequestMapping(value = "/news" , method = RequestMethod.GET)
	public String getNews(Model model){
		model.addAttribute("menuDefault", menuService.getMenuByType("default"));
        model.addAttribute("menuNew", menuService.getMenuByType("new"));
		Long rowCount = eventService.getRowCount();
		Long paging = rowCount/5;
		List<Long> lsPaging = new ArrayList<Long>();
		if(0 == paging){
			lsPaging.add(++paging);
		}else if(paging >= 1) {
			paging = paging + 1;
			for (long i = 1; i <= paging; i++) {
				lsPaging.add(i);
			}
		}
		List<NewsEvent> listNews = eventService.getDataPaging(0, 5);
		if(listNews != null && !listNews.isEmpty()){
			model.addAttribute("listNews", listNews);
			model.addAttribute("lsPaging", lsPaging);
			model.addAttribute("pageCurrent", 1);
		}
		return "newshome";
	}
	@RequestMapping(value = "/news/page/{pagenumber}" , method = RequestMethod.GET)
	public String getNewsByPage(Model model, @PathVariable("pagenumber") int pagenumber){
		model.addAttribute("menuDefault", menuService.getMenuByType("default"));
        model.addAttribute("menuNew", menuService.getMenuByType("new"));
		Long rowCount = eventService.getRowCount();
		Long paging = rowCount/5;
		if(pagenumber > paging +1){
			return "error";
		}
		List<Long> lsPaging = new ArrayList<Long>();
		if(0 == paging){
			lsPaging.add(++paging);
		}else if(paging >= 1) {
			paging = paging + 1;
			for (long i = 1; i <= paging; i++) {
				lsPaging.add(i);
			}
		}
		List<NewsEvent> listNews = eventService.getDataPaging(pagenumber-1, 5);
		if(listNews != null && !listNews.isEmpty()){
			model.addAttribute("listNews", listNews);
			model.addAttribute("lsPaging", lsPaging);
			model.addAttribute("pageCurrent", pagenumber);
		}
		return "newshome";
	}
	
	@RequestMapping(value = "/news/{alias}", method = RequestMethod.GET)
	public String detailNews2(Model model,@PathVariable("alias") String alias, HttpServletRequest request) throws UnsupportedEncodingException{
		model.addAttribute("menuDefault", menuService.getMenuByType("default"));
        model.addAttribute("menuNew", menuService.getMenuByType("new"));
        int hyphen = alias.lastIndexOf(Constants.HYPHEN);
        String encodeId = alias.substring(hyphen+1, alias.length());
        String id = encodeId.substring(8, encodeId.length()-3);
		NewsEvent newsEvent = eventService.findbyId(Integer.valueOf(id));
		model.addAttribute("newsEvent", newsEvent);
		if(Constants.EVENT == newsEvent.getType()){
			if(newsEvent.getGallery() != null){
				String gallery = newsEvent.getGallery();
				String[] lsPath = gallery.split(Constants.COMMA);
				List<String> lsImages = new ArrayList<String>();
				for (int i = 0; i < lsPath.length; i++) {
					lsImages.add(lsPath[i]);
				}
				if(!lsImages.isEmpty()){
					model.addAttribute("gallery",lsImages);
				}
			}
			return "newshomeevent";
		} else {
			return "newshomearticle";
		}
    }
	
	
	
	@RequestMapping(value ="/eventdata", method = RequestMethod.GET)
	public String showEvent(Model model){
		List<NewsEvent> listEvent = eventService.getDataPaging(1,5);
		model.addAttribute("listEvent", listEvent);
		return "eventdata";
	}
	
	@RequestMapping(value = { "/admin/news" }, method = RequestMethod.GET)
	public String news(Model model) {
		List<NewsEvent> listNewEvent = eventService.getAllData();
		model.addAttribute("listNews", listNewEvent);
		return "news";
	}
	
	@RequestMapping(value = "/admin/deletenews", method = RequestMethod.POST)
	public String deleteNews(Model model, @RequestParam("id") int id, HttpServletRequest request) throws IOException{
		NewsEvent deleteNews = eventService.findbyId(id);
		String path = deleteNews.getPath();
		if(deleteNews != null){
			eventService.deleteNews(deleteNews);
		}
		FileUtils.deleteFolderNews(request, path);
		
		return "redirect:/admin/news";
	}

	@RequestMapping(value = "/admin/saveevent", method = RequestMethod.POST)
	public String uploadFile(Model model, 
			@RequestParam("files") MultipartFile[] files,
			@RequestParam("feature") MultipartFile feature,
			@RequestParam("title") String title,
			@RequestParam("description") String description,
			@RequestParam("short_des") String short_des, HttpServletRequest request) {
		String eventFolder = title;
		Date date = new Date();
		if(title.length() > 5){
			eventFolder = title.substring(0, 4).trim().concat(String.valueOf(date.getTime()));
		}
		//save file Feature Image
		StringBuilder mainImage = new StringBuilder();
		mainImage.append(eventFolder);
		mainImage.append(File.separator);
		mainImage.append(feature.getOriginalFilename());
		FileUtils.saveFileGallery(request, feature, eventFolder, feature.getOriginalFilename());
		
		StringBuilder gallerysb = new StringBuilder();
		if (files != null) {
			for (MultipartFile multipartFile : files) {
				String name = multipartFile.getOriginalFilename();
				gallerysb.append(eventFolder);
				gallerysb.append(File.separator);
				gallerysb.append(name);
				gallerysb.append(Constants.COMMA);
				FileUtils.saveFileGallery(request, multipartFile, eventFolder, name);
			}
		}
		String gallery = gallerysb.toString().substring(0,gallerysb.toString().length()-1);
		
		NewsEvent newevent = new NewsEvent();
		newevent.setDescription(description);
		newevent.setTitle(title.trim());
		newevent.setShort_description(short_des);
		newevent.setType(Constants.EVENT);
		newevent.setFeature_image(mainImage.toString());
		newevent.setDate_create(date);
		newevent.setGallery(gallery);
		newevent.setPath(eventFolder);
		eventService.saveEvent(newevent);
		return "redirect:/admin/news";
	}
	
	@RequestMapping(value = "/admin/savearticle", method = RequestMethod.POST)
	public String saveArticle(Model model, 
			@RequestParam("type") int type,
			@RequestParam("feature") MultipartFile feature,
			@RequestParam("title") String title,
			@RequestParam("description") String description,
			@RequestParam("short_des") String short_des, HttpServletRequest request) {
		String fileFolder = title;
		Date date = new Date();
		if(title.length() > 5){
			fileFolder = title.substring(0, 4).trim().concat(String.valueOf(date.getTime()));
		}
		//save file Feature Image
		StringBuilder mainImage = new StringBuilder();
		mainImage.append(fileFolder);
		mainImage.append(File.separator);
		mainImage.append(feature.getOriginalFilename());
		FileUtils.saveFileGallery(request, feature, fileFolder, feature.getOriginalFilename());
		NewsEvent newevent = new NewsEvent();
		newevent.setDescription(description);
		newevent.setTitle(title.trim());
		newevent.setShort_description(short_des);
		newevent.setType(type);
		newevent.setFeature_image(mainImage.toString());
		newevent.setDate_create(date);
		newevent.setPath(fileFolder);
		eventService.saveEvent(newevent);
		return "redirect:/admin/news";
	}
	
	@RequestMapping(value = "/admin/images", method = RequestMethod.POST)
	@ResponseBody
	public String handleTinyMCEUpload(@RequestParam("files") MultipartFile file, HttpServletRequest request) {
		FileUtils.uploadFilesFromTinyMCE(request, file);
		StringBuilder pathFile = new StringBuilder();
		pathFile.append(Constants.SLASH);
		pathFile.append(Constants.FOLDER_ASSET);
		pathFile.append(Constants.SLASH);
		pathFile.append(Constants.TINYMCE);
		pathFile.append(Constants.SLASH);
		pathFile.append(file.getOriginalFilename());
	    return pathFile.toString();

	}
	
}
