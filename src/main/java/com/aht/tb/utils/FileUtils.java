package com.aht.tb.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {
    private static final Logger logger = Logger.getLogger(FileUtils.class);

    /**
     * read propeties file
     * 
     * @param properties
     * @return
     */
    public static String loadProperties(String properties) {
        Properties prop = new Properties();
        InputStream input = null;
        String value = Constants.EMPTY_STRING;
        try {
            input = new FileInputStream("application.properties");

            // load properties
            prop.load(input);
            value = prop.getProperty(properties);
        } catch (Exception e) {
            logger.error("error load file: " + e.getMessage());
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    logger.error("error close file: " + ex.getMessage());
                }
            }
        }
        return value;
    }

    /**
     * function to load file in folder static/image
     * 
     * @param fileName
     * @return
     */
    public static byte[] testLoadImage(String fileName) {
        ClassLoader classloader = Thread.currentThread()
                .getContextClassLoader();
        InputStream is = classloader.getResourceAsStream("static/images/"
                + fileName);
        try {
            byte[] byteImage = IOUtils.toByteArray(is);
            return byteImage;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File getFolderUpload(HttpServletRequest request) {

        String uploadRootPath = request.getServletContext()
                .getRealPath("media")
                + File.separator
                + "images"
                + File.separator + "img_service";

        File folderUpload = new File(uploadRootPath);
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }

    // save image
    public static void saveFolderImagesPath(HttpServletRequest request,
            String imageName, byte[] arrayImage) {
        try {
            String uploadRootPath = request.getServletContext().getRealPath(
                    "media")
                    + File.separator + "images";
            String fileType = ".png";
            String filename = uploadRootPath + File.separator + imageName;
            if (!imageName.contains(".")) {
                filename = filename.concat(fileType);
            }
            File file = new File(filename);
            if (file.exists()) {
                file.deleteOnExit();
                org.apache.commons.io.FileUtils.writeByteArrayToFile(file,
                        arrayImage);
            } else {
                org.apache.commons.io.FileUtils.writeByteArrayToFile(file,
                        arrayImage);
            }
        } catch (Exception e) {
            logger.error("File exception: " + e.getMessage());
        }
    }
    public static void saveFolderImages(HttpServletRequest request,
            String imageName, MultipartFile fileImage) {
        try {
            String uploadRootPath = request.getServletContext().getRealPath(
                    "media")
                    + File.separator + "images";
            String fileType = ".png";
            String filename = uploadRootPath + File.separator + imageName;
            if (!imageName.contains(".")) {
                filename = filename.concat(fileType);
            }
            File file = new File(filename);
            fileImage.transferTo(file);
        } catch (Exception e) {
            logger.error("File exception: " + e.getMessage());
        }
    }
    
    /**
     * 
     * @param request
     * @return
     */
    public static String getPathFilePDF(HttpServletRequest request){
        String uploadFilePDF = request.getServletContext().getRealPath("documents")+File.separator + "brochures";
        File file = new File(uploadFilePDF);
        if(!file.exists()){
            file.mkdir();
        }
        return uploadFilePDF.concat(File.separator);
    }
    
    public static String getPathNewsFolder(HttpServletRequest request, String folderName){
    	String path = request.getServletContext().getRealPath("assets").concat(File.separator).concat(folderName);
    	File file = new File(path);
    	if(!file.exists()){
    		file.mkdir();
    	}
    	return path.concat(File.separator);
    }
    
    public static void uploadFilesFromTinyMCE(HttpServletRequest request,MultipartFile fileImage){
    	String path = request.getServletContext().getRealPath("assets").concat(File.separator).concat(Constants.TINYMCE);
    	File fileFolder = new File(path);
    	if(!fileFolder.exists()){
    		fileFolder.mkdir();
    	}
    	String fileName = fileImage.getOriginalFilename();
    	path = path.concat(File.separator).concat(fileName);
    	File fileImageSave = new File(path);
    	try {
    		fileImage.transferTo(fileImageSave);
		} catch (Exception e) {
			logger.equals("error save file: " + e.getMessage());
		}
    }
    
    public static void deleteFolderNews(HttpServletRequest request, String folderName) throws IOException{
    	String path = request.getServletContext().getRealPath("assets").concat(File.separator).concat(folderName);
    	File file = new File(path);
    	if(file.exists()){
    		org.apache.commons.io.FileUtils.deleteDirectory(file);
    	}
    }
    
    public static void saveFileGallery(HttpServletRequest request,MultipartFile fileImage, String folderName, String fileName){
    	try {
			String filePath = getPathNewsFolder(request, folderName);
			filePath = filePath.concat(fileName);
			File file = new File(filePath);
			fileImage.transferTo(file);
		} catch (Exception e) {
			logger.debug("error save file : " + e.getMessage());
		}
    }
    /**
     * 
     * @param request
     * @param filePdf
     */
    public static void savefileBrochure(HttpServletRequest request, MultipartFile filePdf, String filePdfDB){
        try {
            String uploadPath= getPathFilePDF(request);
            uploadPath = uploadPath.concat(filePdfDB);
            File file = new File(uploadPath);
            filePdf.transferTo(file);
        } catch (Exception e) {
            logger.debug("error save file : " + e.getMessage());
        }
    }
    
    /**
     * 
     * @param request
     * @param filesaved
     */
    public static void deleteFileBrochure(HttpServletRequest request, String filesaved){
        try {
            String uploadPath= getPathFilePDF(request);
            uploadPath = uploadPath.concat(filesaved);
            File file = new File(uploadPath);
            if(file.exists()){
                file.delete();
            }
        } catch (Exception e) {
            logger.debug("error delete file : " + e.getMessage());
        }
    }
    
    //delete image
    public static boolean deleteImageFile(HttpServletRequest request,
            String imageName) {
        String uploadRootPath = request.getServletContext().getRealPath(
                "media")
                + File.separator + "images";
        String filename = uploadRootPath + File.separator + imageName;
        File file = new File(filename);
        if (file.exists()) {
            file.delete();
            return true;
        }
        return false;
    }
    // save video
    public static void saveVideo(HttpServletRequest request, String filename,
            MultipartFile multipartFile, String foldername) {
        try {
            String originalFileName = multipartFile.getOriginalFilename();
            String[] splitFile = originalFileName.split(Pattern.quote("."));
            String fileType = splitFile[1];
            String uploadRootPath = request.getServletContext().getRealPath(
                    "media")
                    + File.separator + "video";
            if (!foldername.isEmpty()) {
                uploadRootPath = uploadRootPath + File.separator + foldername;
            }
            String fileNameLatest = filename;
            if (!filename.contains(fileType)) {
                fileNameLatest = filename + "." + fileType;
            }
            File videoFile = new File(uploadRootPath, fileNameLatest);
            multipartFile.transferTo(videoFile);
        } catch (Exception e) {
            logger.error("File exception: " + e.getMessage());
        }
    }

    /**
     * encode image to display in jsp
     * 
     * @param image
     * @return
     */
    public static String encodeImage(byte[] image) {
        String base64Encoded = Constants.EMPTY_STRING;
        if (image != null && image.length > 0) {
            try {
                byte[] encodeBase64 = Base64.encodeBase64(image);
                base64Encoded = new String(encodeBase64, "UTF-8");
                return base64Encoded;
            } catch (Exception e) {
                logger.error("encode image error: " + e.getMessage());
            }
        }
        return base64Encoded;
    }
}
