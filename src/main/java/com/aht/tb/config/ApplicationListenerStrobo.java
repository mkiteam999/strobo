package com.aht.tb.config;


import org.apache.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

public class ApplicationListenerStrobo implements ApplicationListener<ContextClosedEvent>{

    private static final Logger logger =  Logger.getLogger(ApplicationListenerStrobo.class);
    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        logger.debug("closes app");
    }
    
}
