package com.aht.tb.config;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.aht.tb.entity.ConfigPage;
import com.aht.tb.entity.Contact;
import com.aht.tb.entity.GGCaptcha;
import com.aht.tb.entity.HomePage;
import com.aht.tb.entity.Menu;
import com.aht.tb.entity.PageInfo;
import com.aht.tb.entity.Role;
import com.aht.tb.entity.ServiceApp;
import com.aht.tb.entity.User;
import com.aht.tb.repository.ConfigRepository;
import com.aht.tb.repository.ContactRepository;
import com.aht.tb.repository.GGCaptchaRepository;
import com.aht.tb.repository.HomePageRepository;
import com.aht.tb.repository.MenuRepository;
import com.aht.tb.repository.PageInfoRepository;
import com.aht.tb.repository.RoleRepository;
import com.aht.tb.repository.ServiceRepository;
import com.aht.tb.repository.UserRepository;
import com.aht.tb.utils.Constants;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {
    
    private static final Logger logger = Logger.getLogger(DataSeedingListener.class);
    
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private MenuRepository menuRepository;

	@Autowired
	private ServiceRepository serviceRepository;

	@Autowired
	PageInfoRepository pageInfoRepository;

	@Autowired
	HomePageRepository homePageRepository;

	@Autowired
	ContactRepository contactRepository;

	@Autowired
	ConfigRepository configRepository;

	@Autowired
	private GGCaptchaRepository ggCaptchaRepository;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
	    logger.debug("Start onApplicationEvent !");
		// Role
		if (roleRepository.findByName("ROLE_ADMIN") == null) {
			roleRepository.save(new Role("ROLE_ADMIN"));
		}

//		if (roleRepository.findByName("ROLE_MAINTAIN") == null) {
//			roleRepository.save(new Role("ROLE_MAINTAIN"));
//		}

		// Admin account
		if (userRepository.findByUsername("admin") == null) {
			User admin = new User();
			admin.setId(1);
//			String adminUserName = FileUtils.loadProperties(Constants.ADMIN_USER_NAME);
//			if(StringUtils.isEmpty(adminUserName)){
			String adminUserName = Constants.ADMIN_DEFAULT_AUTHEN;
//			}
//			String adminPassword = FileUtils.loadProperties(Constants.ADMIN_USER_PASSWORD);
//			if(StringUtils.isEmpty(adminUserName)){
			String adminPassword = Constants.ADMIN_DEFAULT_AUTHEN;
//            }
			admin.setUsername(adminUserName);
			admin.setPassword(passwordEncoder.encode(adminPassword));
			admin.setEmail("strobo@stengg.com");

			HashSet<Role> roles = new HashSet<>();
			roles.add(roleRepository.findByName("ROLE_ADMIN"));
			admin.setRoles(roles);
			try {
				userRepository.save(admin);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Service Page Info
		if (pageInfoRepository.findAll().size() == 0) {
			PageInfo pageInfo = new PageInfo();
			pageInfo.setId(1);
			pageInfo.setStatus(true);
			pageInfoRepository.save(pageInfo);
		}

		// Home Page
		if (homePageRepository.findAll().isEmpty()) {
			HomePage homePage = new HomePage();

			// Get current time set for createdDate
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String day = dateFormat.format(date);
			homePage.setCreatedDate(day);

			homePage.setKeyword("");
			homePage.setMeta("");
			homePage.setPageName("");
			homePage.setStatus(true);
			homePage.setTitle("");
			homePage.setButtonLabel("WATCH VIDEO");
			homePage.setButtonLink("#");
			homePage.setFile("");
			homePage.setHeading1("Future-Ready Autonomous Solutions");
			homePage.setHeading2("Logistics");
			homePage.setHeading3("Effortlessly");
			homePageRepository.save(homePage);
		}

		// Service Page
		if (serviceRepository.findAll().isEmpty()) {
			ServiceApp serviceApp = new ServiceApp();
			String[] title = {"Seaport" , "Manufacturing", "Warehourse", "Airport" };
			String[] imageName = { "Seaport.png", "Manufacturing.png", "Warehourse.png", "Airport.png" };
			String[] linkvideo = { "", "", "", "" };
			String content = "lorem ipsum dolor sit amet, cons adipiscing elit, sed do eiusmod";
			for (int i = 1; i <= 4; i++) {
				serviceApp.setId(i);
				serviceApp.setContentService(content);
				serviceApp.setTitleService(title[i - 1]);
				serviceApp.setImageService(imageName[i - 1]);
				serviceApp.setLinkvideo(linkvideo[i - 1]);
				serviceApp.setPageInfo(pageInfoRepository.findAll().get(0));
				serviceRepository.save(serviceApp);
			}
		}

		// Contact Us
		if (contactRepository.findAll().isEmpty()) {
			Contact contact = new Contact();

			// Get current time set for createdDate
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String day = dateFormat.format(date);
			contact.setStatus(true);
			contact.setCreatedDate(day);
			contact.setStatus(true);
			contact.setEmail("enquiry_strobo@stengg.com");
			contact.setPhone("(65) 6663 1855");
			contact.setAddress("");
			contact.setInstagram("#");
			contact.setFacebook("#");
			contact.setTwitter("#");
			contact.setGoogleApi("ST Engineering Land Systems Ltd. 15 Chin Bee Drive, Singapore 619863");
			contactRepository.save(contact);
		}

		// Config page
		if (configRepository.findAll().isEmpty()) {
			ConfigPage configPage = new ConfigPage();
			// maintenance
			configPage.setMode(false);
			configPage.setContent("Maintenance Mode");

			// config page
			configPage.setError_head("404 not found!");
			configPage.setError_content("Error! Not found!");
			configRepository.save(configPage);
		}

		// Menu
		if (menuRepository.findAll().isEmpty()) {
			String[] menus = { "Home", "About", "Our Core Technologies", "Seaport", "Manufacturing", "Warehouse", "Airport", "News", "Contact Us" };
			String[] url = { "#home", "#about", "#core", "#seaport", "#manufacturing", "#warehouse","#airport", "#news", "#contact" };
			for (int i = 0; i < menus.length; i++) {
				menuRepository.save(new Menu(menus[i], url[i], true, "default"));
			}
		}

		// Google reCaptcha
		if (ggCaptchaRepository.findAll().isEmpty()) {
			ggCaptchaRepository.save(new GGCaptcha(1, false, null));
		}

	}

}
