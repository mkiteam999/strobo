package com.aht.tb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;



@SpringBootApplication
public class TbApplication{
    
    private static final Logger logger = Logger.getLogger(TbApplication.class); 
    
    @Bean
    MyBean myBean() {
        return new MyBean();
    }
    
	public static void main(String[] args) {
	    SpringApplication.run(TbApplication.class, args);
	}
	
	
    private static class MyBean {

        @PostConstruct
        public void init() {
            logger.debug("start app");
        }

        @PreDestroy
        public void destroy() {
            logger.debug("destroy!!!");
        }
    }
	
}
